<div class="gbc-block">

   <b>Enable/Disable NightMode</b>
    <hr>
    <p>NightMode is <span class="c-status"><?=($app_settings->app_status=='0') ? 'disabled' :'enabled'?></span> on your store currently.</p>
    <label for="enable-app"><input id="enable-app" <?php echo ($app_settings->app_status=="1") ? 'checked':''?> class="app-ed-setting" type="radio" name="setting" value="1"> Enable</label>
    
    <label for="disable-app"><input id="disable-app" <?php echo ($app_settings->app_status=="0") ? 'checked':''?> class="app-ed-setting" type="radio" name ="setting" value="0"> Disable</label>
    <button style="float:right;min-width:100px" id="save-settings" class="pcc-btn">Save</button>
    <div>Note : After Enable/Disable app, Store page reload is required to reflect the changes.</div>
</div>


<!-- <div class="gbc-block">



    <form method="post" action="<?=SITE_URL?>/setting.php" id="save_nm_settings" enctype="multipart/form-data">

        <h2>App settings</h2>
            <input type="hidden" name="store_domain" value="<?= $store ?>"> 

            <input type="hidden" name="ac" value="update_setting"> 

            <label>Include Pages</label><br>                               


            <input type="checkbox"  name="homepage" value="Homepage" <?php// if($app_settings->include_pages->homepage == 1) echo "checked";  ?> >
            <label for="vehicle1">Homepage</label><br>


            <input type="checkbox"  name="collection" value="Collection" <?php //if($app_settings->include_pages->collection == 1) echo "checked";  ?>>
            <label for="vehicle2">Collection Page</label><br>


            <input type="checkbox"  name="product" value="Products" <?php// if($app_settings->include_pages->products == 1) echo "checked";  ?>>
            <label for="vehicle3">Products</label><br><br>


            





        <?php //if ( $app_charge_status == '0'){ ?>
           


        <?php //}else{?>


                <label>App logo</label><br>
                <img id="logo_img" src="<?=($app_settings->logo) ?>">

                <input type="file" name="image" accept="image/x-png,image/jpeg" onchange="loadFile(event,'logo_img')" />    <br> <br><br>  


                 <label>Please select button position:</label><br>

                <input required type="radio" name="botton_position" value="top_right" <?php //if($app_settings->button_position == "top_right") echo "checked";  ?> >
                <label for="male">Top Right</label><br>

                <input required type="radio" name="botton_position" value="top_left" <?php// if($app_settings->button_position == "top_left") echo "checked";  ?> >
                <label for="female">Top Left</label><br>

                <input required type="radio" name="botton_position" value="bottom_right" <?php// if($app_settings->button_position == "bottom_right") echo "checked";  ?> >
                <label for="other">Bottom Right</label> <br>

                <input required type="radio" name="botton_position" value="bottom_left" <?php //if($app_settings->button_position == "bottom_left") echo "checked";  ?> >
                <label for="other">Bottom Left</label> <br>      
                                  

        <?php// } ?>



        <button id="premium-upgrade" type="submit" name="upgrade" class="btn-primary" >Save settings</button>

    </form>
</div> -->


<!-- <div class="gbc-block">

    <h2>Exclude Elements</h2><br>
    <p>Please provide your site's page link where you want to exclude elements</p><br>
    <input required type="text" name="exclude_elements" class="exclude_elements">
    <button class="exclude_elm_btn">Proceed</button>
</div> -->

<div class="gbc-block">
    <div class="row">
        <div class="col-12">
            <b>How to use</b>
            <hr>
            1.) Install the NightMode app. <b style="color:#91c516">You Already Did That!</b><br>
            2.) Now enable the app from above & its <b>Done</b><br>
            3.) Right bottom of the Site screen a button will be there to enable or disable Nightmode.<br>
        </div>
    </div>
</div>

<div class="gbc-block">
    <p><b>Feedbacks are most welcome</b></p>
    <hr>
    <textarea maxlength="1000" style="resize:none;margin-bottom: 20px;" class="form-control" name="feedback" id="" cols="30" rows="2" placeholder="Please send us your valuable feedbacks, Or any suggestions you have for improvements."></textarea>
    <p style="font-size: 12px;margin: 0;font-weight: 600;color: #9e9fa0;">Limit 1000 characters</p>
    <button style="float:right;min-width:100px" class="pcc-btn" id="send-feedback">Send</button>
</div>


<div class="gbc-block">
    <div class="row">
        <div class="col-12">
            

            <!-- <p>Like our service? Leave us a positive review!</p> -->
            <!-- 
            <p><i style="color: #91c516" class="fa fa-gift" aria-hidden="true"></i> Get one month free by leaving us a review!</p>
           
            <p style="margin-bottom: 0">Positive feedback keeps us going! Help by leaving a review on Shopify App Store now!. <a href="https://apps.shopify.com/nightmode#reviews" target="_blank">Click here</a> to leave a review.</p>-->
            
        </div>
    </div>
</div>