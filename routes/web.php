<?php   
        
use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\HomeController; 
//use App\Http\Controllers\Admin\CategoryController;

  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('auth/social', [App\Http\Controllers\Auth\AppLoginController::class, 'show'])->name('social.login');
Route::get('/oauth/{driver}', [App\Http\Controllers\Auth\AppLoginController::class, 'redirectToProvider'])->name('social.oauth');
Route::get('oauth/{driver}/callback', [App\Http\Controllers\Auth\AppLoginController::class, 'handleProviderCallback'])->name('social.callback');

Route::post('/create_link_token', [App\Http\Controllers\Banks::class, 'create_link_token'])->name('createe-token');
Route::post('/exchange_public_token', [App\Http\Controllers\Banks::class, 'exchange_public_token'])->name('exchange-token');
Route::post('/process_plaid_token', [App\Http\Controllers\Banks::class, 'process_plaid_token'])->name('process-token');
Route::post('/create_link_token_v1', [App\Http\Controllers\Banks::class, 'create_link_token_v1'])->name('process-token-v1');
Route::post('/get_balance', [App\Http\Controllers\Banks::class, 'get_balance'])->name('get-balance');



Route::group(['middleware' => 'auth'], function(){

	// Route::get('/', function () {
	//     return view('home');
	// })->name('homeC');

	Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('homeC');
	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::get('/banks', [App\Http\Controllers\Banks::class, 'index'])->name('banks');
	Route::get('/transactions', [App\Http\Controllers\TransactionsController::class, 'index'])->name('transactionsController');
	Route::get('/report', [App\Http\Controllers\ReportController::class, 'index'])->name('report');
	Route::get('/personal-information', [App\Http\Controllers\PersonalInformationController::class, 'index'])->name('personalInformationController');
	Route::get('/tax-payment', [App\Http\Controllers\TaxPayment::class, 'index'])->name('taxPayment');
	Route::get('/change-colors', [App\Http\Controllers\ChangeColorsController::class, 'index'])->name('changeColorsController');
	Route::get('/support', [App\Http\Controllers\SupportController::class, 'index'])->name('supportController');
	Route::get('/report-all-categories', [App\Http\Controllers\ReportAllCategoriesController::class, 'index'])->name('reportAllCategoriesController');
	Route::get('/verification-code', [App\Http\Controllers\VerificationCodeController::class, 'index'])->name('verificationCodeController');
	Route::get('/number-verify', [App\Http\Controllers\NumberVerifyController::class, 'index'])->name('numberVerifyController');
	Route::get('/expense', [App\Http\Controllers\ExpenseController::class, 'index'])->name('expenseController');
	Route::get('/income', [App\Http\Controllers\IncomeController::class, 'index'])->name('incomeController');
	Route::get('/transaction-list', [App\Http\Controllers\TransactionList::class, 'index'])->name('transactionList');

	Route::post('/profile-update', [App\Http\Controllers\HomeController::class, 'upload'])->name('profile-update');
	Route::post('/documents-update', [App\Http\Controllers\HomeController::class, 'document_upload'])->name('documents-update');

	Route::get('/change-password', [App\Http\Controllers\ChangePasswordController::class, 'index'])->name('changePasswordController');
	Route::post('/password-update', [App\Http\Controllers\ChangePasswordController::class, 'password_update'])->name('password-update');

	//Route::get('auth/social', 'Auth\AppLoginController@show')->name('social.login');
	//Route::get('oauth/{driver}', 'Auth\AppLoginController@redirectToProvider')->name('social.oauth');
	//Route::get('oauth/{driver}/callback', 'Auth\AppLoginController@handleProviderCallback')->name('social.callback');

	//Route::get('auth/{driver}', 'Auth\GoogleController@redirectToGoogle');
	//Route::get('auth/{driver}/callback', 'Auth\GoogleController@handleGoogleCallback');
 

});


Route::group([
    'prefix'     => 'admin',
    'as'         => 'admin.',
    'namespace'  => 'Admin',
    'middleware' => ['auth', 'App\Http\Middleware\IsAdminMiddleware']
], function () {

    // Route::get('/dashboard', 'HomeController@admin_index')->name('dashboard');
	Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'admin_index'])->name('dashboard');
	Route::get('/users', [App\Http\Controllers\Admin\UsersController::class, 'index'])->name('users');
	Route::get('/banks', [App\Http\Controllers\Admin\BanksController::class, 'index'])->name('banks');
	Route::get('/transaction', [App\Http\Controllers\Admin\TransactionController::class, 'index'])->name('transaction');
	Route::get('/reports', [App\Http\Controllers\Admin\ReportsController::class, 'index'])->name('reports');
	//Route::get('/categories', [App\Http\Controllers\Admin\CategoriesController::class, 'index'])->name('categories');
	//Route::post('/add-categories', [App\Http\Controllers\Admin\CategoriesController::class, 'add_categories'])->name('add-categories');



    Route::resource('categories', CategoryController::class);

    // // Roles
    // Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    // Route::resource('roles', 'RolesController');


});


