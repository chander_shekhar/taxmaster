<?php
 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use TomorrowIdeas\Plaid\Plaid; 
use TomorrowIdeas\Plaid\Plaid\Resources\AbstractResource;
use TomorrowIdeas\Plaid\Plaid\Resources\Auth;
use DB; 
use Session;

class Banks extends Controller
{

    
    public function index() 
    {      

        $banks_set  = Session::get('banks_set');

        if($banks_set){

            $connected_banks = DB::select('SELECT * from TBL_CONNECTED_ACCOUNTS WHERE user_id ='.auth()->user()->id);

        }else{

            $connected_banks = [];

        }

        return view( 'banks', ['connected_banks' => $connected_banks] );         
    }

    public function create_link_token(Request $request)
    {

    	$curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL             => 'https://sandbox.plaid.com/sandbox/public_token/create',
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "client_id": "611bd1338a49a9000fcdd108",
                "secret": "33c8e3696561047b1c1972326a227b",
                "institution_id": "ins_3",
                "initial_products": ["auth"],
                "options": {
                    "webhook": "https://www.genericwebhookurl.com/webhook"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $public_token = curl_exec($curl);
        //$public_token = json_decode($public_token, true);
        //$exchange_token   = $public_token['public_token'];
        //return view('banks', ['public_token'=>$public_token] );
        return $public_token;
        
    }

    public function create_link_token_v1(Request $request)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sandbox.plaid.com/link/token/create',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "client_id": "611bd1338a49a9000fcdd108",
                "secret": "33c8e3696561047b1c1972326a227b",
                "client_name": "Insert Client name here",
                "country_codes": ["US"],
                "language": "en",
                "user": {
                    "client_user_id": "unique_user_id"
                },
                "products": ["auth"]
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $link_token = curl_exec($curl);
        curl_close($curl);
        //$public_token = json_decode($public_token, true);
        //$exchange_token   = $public_token['public_token'];
        //return view('banks', ['public_token'=>$public_token] );
        return $link_token;
        
    }

    public function exchange_public_token(Request $request)
    {   
        $exchange_token = $request->input('public_token');
        $accounts       = $request->input('accounts');
        $institution    = $request->input('institution');

        $curl = curl_init();
    	curl_setopt_array($curl, array(
            CURLOPT_URL             => 'https://sandbox.plaid.com/item/public_token/exchange',
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => 'POST',
            CURLOPT_POSTFIELDS      =>'{
              "client_id"    :      "611bd1338a49a9000fcdd108",
              "secret"       :      "33c8e3696561047b1c1972326a227b",
              "public_token" :      "'.$exchange_token.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //return $response;  

        $response  = json_decode($response, true);

        $access_token  = $response['access_token'];

        //insert access token as per user and other details 

        //$deleted = DB::delete('DELETE FROM TBL_CONNECTED_ACCOUNTS WHERE user_id = ?',[ auth()->user()->id ] );

        foreach ($accounts as $key => $value) {


            $account_id = $accounts[$key]['id'];
            $name       = $accounts[$key]['name'];
            $subtype    = $accounts[$key]['subtype'];
            $type       = $accounts[$key]['type'];
            $mask       = $accounts[$key]['mask'];

            $institution_name    = $institution['name'];
            $institution_id      = $institution['institution_id'];

            $data       =   array(
                                'user_id'           => auth()->user()->id, 
                                'public_token'      => $access_token, 
                                'account_id'        => $account_id,
                                "name"              => $name,
                                "subtype"           => $subtype,
                                "type"              => $type,
                                "mask"              => $mask,
                                "institution_name"  => $institution_name,
                                "institution_id"    => $institution_id
                            );
                
            DB::table('TBL_CONNECTED_ACCOUNTS')->insert($data);
            
        }

        Session::put('banks_set', true);

        echo "data inserted";

    }

    // public function get_balance(Request $request)
    // {   

    //     $access_token   = $request->input('access_token');

    //     $curl           = curl_init();

    //     curl_setopt_array($curl, array(
    //         CURLOPT_URL => 'https://sandbox.plaid.com/accounts/balance/get',
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_ENCODING => '',
    //         CURLOPT_MAXREDIRS => 10,
    //         CURLOPT_TIMEOUT => 0,
    //         CURLOPT_FOLLOWLOCATION => true,
    //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //         CURLOPT_CUSTOMREQUEST => 'POST',
    //         CURLOPT_POSTFIELDS =>'{
    //           "client_id"    : "611bd1338a49a9000fcdd108",
    //           "secret"       : "33c8e3696561047b1c1972326a227b",
    //           "access_token" : "'.$access_token.'"
    //         }',
    //         CURLOPT_HTTPHEADER => array(
    //             'Content-Type: application/json'
    //         ),
    //     ));

    //     $response = curl_exec($curl);

    //     curl_close($curl);
    //     //echo $response;

    //     $json_response = $response;
    //     $response = json_decode($response,true);

    //     if ( count($response['accounts']) > 0 ) {

    //         $deleted = DB::delete('DELETE FROM TBL_CONNECTED_ACCOUNTS WHERE user_id = ?',[ auth()->user()->id ] );

    //         foreach ($$response['accounts'] as $key => $value) {


    //             $account_id                     = $accounts[$key]['account_id'];
    //             $name                           = $accounts[$key]['name'];
    //             $official_name                  = $accounts[$key]['official_name'];
    //             $subtype                        = $accounts[$key]['subtype'];
    //             $type                           = $accounts[$key]['type'];

    //             $balance_available              = $accounts[$key]['balance']['available'];
    //             $balance_current                = $accounts[$key]['balance']['current'];
    //             $iso_currency_code              = $accounts[$key]['balance']['iso_currency_code'];
    //             $account_limit                  = $accounts[$key]['balance']['limit'];
    //             $unofficial_currency_code       = $accounts[$key]['balance']['unofficial_currency_code'];

    //             $data                           =   array(
    //                                                     'user_id'                  => auth()->user()->id, 
    //                                                     'public_token'             => $access_token, 
    //                                                     'account_id'               => $account_id,
    //                                                     "balance_available"        => $balance_available
    //                                                     "balance_current"          => $balance_current
    //                                                     "iso_currency_code"        => $iso_currency_code
    //                                                     "account_limit"            => $account_limit 
    //                                                     "unofficial_currency_code" => $unofficial_currency_code
    //                                                     "name"                     => $name,
    //                                                     "subtype"                  => $subtype,
    //                                                     "type"                     => $type
    //                                                 );
                    
    //             DB::table('TBL_CONNECTED_ACCOUNTS')->insert($data);

    //             echo $json_response;
                
    //         }
           
    //     }

    // }


}