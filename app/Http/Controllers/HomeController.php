<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard. 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        return view('home');
    }

    public function upload(Request $request)
    {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required'
        ]);

       
        $image = $request->file('photo');
        if ($image != null) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension(); 
            $image->move(public_path('images/profile'), $new_name);
            Auth()->user()->update(['profile_image'=>"/images/profile/".$new_name]);

        }

        $first_name          = $request->input('first_name');
        $last_name           = $request->input('last_name');
        $tax_payer           = $request->input('tax_payer');
        $webfile             = $request->input('webfile');
        $ein                 = $request->input('ein');
        $company_start_date  = $request->input('company_start_date');


        $timestamp = strtotime($company_start_date); 
        $new_date = date('Y-m-d', $timestamp);

        Auth()->user()->update(['first_name'=>$first_name]);
        Auth()->user()->update(['last_name'=>$last_name]);
        Auth()->user()->update(['tax_payer'=>$tax_payer]);
        Auth()->user()->update(['webfile'=>$webfile]);
        Auth()->user()->update(['ein'=>$ein]);
        Auth()->user()->update(['company_start_date'=>$new_date ]);        

        return redirect()->back()->with('success', 'Profile Updated');
    }

    public function document_upload(Request $request)
    {

        $this->validate($request, [
            'document_type' => 'required',
            'document_comments' => 'required',
            'fileUpload' => 'required'
        ]);

       
        $image = $request->file('fileUpload');
        if ($image != null) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension(); 
            $image->move(public_path('images/documents'), $new_name);
            $img_name = "/images/documents/".$new_name;
        }else{
            $img_name = "";
        }

        $document_type       = $request->input('document_type');
        $document_comments   = $request->input('document_comments');

        $data                =array('user_id'=>auth()->user()->id, 'document_type'=>$document_type,"document_comments"=>$document_comments,"document_files"=>$img_name);
            
        DB::table('user_documents')->insert($data);

        return redirect()->back()->with('success', 'Profile Updated');
    }

    public function admin_index(Request $request)
    {

        //$search =  trim( $request->input('search') ) ;


        $users_latest = DB::select('SELECT * FROM users WHERE is_admin = 0 ORDER BY id DESC LIMIT 8');
        $banks_latest = DB::select('SELECT * FROM banks ORDER BY id DESC LIMIT 8');

        $users        = DB::select("SELECT SUM(IF(weekday = '1', total_users, 0)) AS 'Sunday',SUM(IF(weekday = '2', total_users, 0)) AS 'Monday',SUM(IF(weekday = '3', total_users, 0)) AS 'Tuesday',SUM(IF(weekday = '4', total_users, 0)) AS 'Wednesday',SUM(IF(weekday = '5', total_users, 0)) AS 'Thursday',SUM(IF(weekday = '6', total_users, 0)) AS 'Friday',SUM(IF(weekday = '7', total_users, 0)) AS 'Saturday',COUNT(*) AS total_users FROM ( SELECT DAYOFWEEK(DATE(created_at)) AS weekday, COUNT(*) AS total_users FROM users WHERE created_at <= NOW() and created_at >= Date_add(Now(),interval - 7 DAY) GROUP BY DAYOFWEEK(DATE(created_at))  ) as sub");

        $banks        = DB::select("SELECT SUM(IF(weekday = '1', total_users, 0)) AS 'Sunday',SUM(IF(weekday = '2', total_users, 0)) AS 'Monday',SUM(IF(weekday = '3', total_users, 0)) AS 'Tuesday',SUM(IF(weekday = '4', total_users, 0)) AS 'Wednesday',SUM(IF(weekday = '5', total_users, 0)) AS 'Thursday',SUM(IF(weekday = '6', total_users, 0)) AS 'Friday',SUM(IF(weekday = '7', total_users, 0)) AS 'Saturday',COUNT(*) AS total_users FROM ( SELECT DAYOFWEEK(DATE(created_at)) AS weekday, COUNT(*) AS total_users FROM banks WHERE created_at <= NOW() and created_at >= Date_add(Now(),interval - 7 DAY) GROUP BY DAYOFWEEK(DATE(created_at))  ) as sub");


           
        return view('admin.dashboard',['users'=>$users,'users_latest'=>$users_latest,'banks_latest'=>$banks_latest,'banks'=>$banks ]); 
    }

}
