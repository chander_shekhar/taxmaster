<?php
  
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReportController extends Controller
{
    public function index()
    {
        $cats  = DB::select('SELECT * from categories WHERE is_deleted = 0 ORDER BY id DESC');  
        return view( 'report', [ 'categories' => $cats ]  );         
    }
}
 