<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
    	return view('change-password');
        
    }

    public function password_update(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'confirm_password' => ['same:new_password'],
        ]);
   
        Auth()->user()->update(['password'=> Hash::make($request->new_password)]);
   
        // dd('Password changed successfully.');

        return redirect()->back()->with('success', 'Password Updated successfully');
    }



}
