<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
use DB;

class PersonalInformationController extends Controller
{
    public function index()
    {
    	$users_docs = DB::select('SELECT * from user_documents WHERE user_id ='.auth()->user()->id);
    	return view( 'personal-information', ['users_docs'=>$users_docs] );       
    }
}
