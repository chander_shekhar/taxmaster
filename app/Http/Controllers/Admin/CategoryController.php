<?php  

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categories;
use Illuminate\Support\Facades\Input;
use DB;

class CategoryController extends Controller
{
    
    public function index(Request $request ){

        $search      = trim( $request->input('search') ) ;

        $categories  = Categories::where('title','like','%'.$search.'%')->latest()->paginate(5);        

        //$categories = Categories::latest()->paginate(5);    

        return view('admin.categories',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 

    public function store(Request $request){

        $request->validate([
            'title' => 'required',
            'image' => 'required',
        ]);  


        $title = trim($request->input('title'));
        $prev_categories = Categories::where('title', '=', $title )->first();
        if ($prev_categories === null) {
           
            $image = $request->file('image');

            if ($image != null) {
                $new_name = rand() . '.' . $image->getClientOriginalExtension(); 
                $image->move(public_path('images/categories'), $new_name);
                $img_name = "/images/categories/".$new_name;
            }else{
                $img_name = "";
            }

            $categories = new Categories;

            $categories->title   = $request->input('title');
            $categories->image   = $img_name;

            $categories->save();

            return redirect()->route('admin.categories.index')
                ->with('success','Category created successfully.');

        }else{

            return redirect()->route('admin.categories.index')
                ->with('warning','Category with same name already exist.');

        }

    }     

    public function show(Categories $categories){

        return view('admin.categories.show',compact('categories'));
    } 

    public function edit(Request $request, Categories $categories){

        $id = app('request')->segment(3);

        $category = DB::select('SELECT * from categories WHERE id = '.$id);
           
        return view('admin.category-edit', ['category'=>$category] );
    }    

    public function update(Request $request, Categories $categories){

        $request->validate([
            'title' => 'required'            
        ]);   

        $id   = $request->input('id'); 

        $title = trim($request->input('title'));

        $prev_categories = DB::select('SELECT * from categories WHERE title = "'.$title.'" AND NOT (id = '.$id.') ' );

        if ( sizeof($prev_categories) == 0 ) {

            $image = $request->file('image');

            if ($image != null) {
                $new_name = rand() . '.' . $image->getClientOriginalExtension(); 
                $image->move(public_path('images/categories'), $new_name);
                $img_name = "/images/categories/".$new_name;
            }else{
                $img_name = "";
            }

            if($img_name){

                DB::update('UPDATE categories SET title = ?, image = ? WHERE id = ?',[$title, $img_name , $id]);

            }else{

                DB::update('UPDATE categories SET title = ? WHERE id = ?',[$title, $id]);
            }


            return redirect()->route('admin.categories.index')
                ->with('success','Category updated successfully');

        }else{

            return redirect()->back()
                ->with('warning','Category with same name already exist.');
        }

        $categories->update($request->all());    

    }

    
    public function destroy(Request $request, Categories $categories){

        $id   = $request->input('id');
        DB::table('categories')->where('id', $id)->delete();
        return redirect()->route('admin.categories.index')
            ->with('success','Category deleted successfully');
    }
}
