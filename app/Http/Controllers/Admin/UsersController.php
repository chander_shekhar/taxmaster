<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class UsersController extends Controller 
{
    public function index(Request $request ){

	    $search =  trim( $request->input('search') ) ;

        //DB::enableQueryLog();


	    $users  = User::where('is_admin', '=', 0 )  					
					
					->where(function($query) use ($search){

				        $query->where('first_name','like','%'.$search.'%')   

						->orWhere('last_name','like','%'.$search.'%')

						->orWhere('phone','like','%'.$search.'%')

						->orWhere('email','like','%'.$search.'%')

						->orWhere('tax_payer','like','%'.$search.'%')

						->orWhere('webfile','like','%'.$search.'%')

						->orWhere('ein','like','%'.$search.'%');
					        
				    })->latest()->paginate(5);

		//print_r(DB::getQueryLog());

		//die('dsfdsfds');

        return view('admin.users',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }
} 
