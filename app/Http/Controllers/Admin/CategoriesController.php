<?php
  
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Categories;

class CategoriesController extends Controller
{
    public function index(){

        $categories = Categories::latest()->paginate(5);    
        return view('admin.categories',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } 

    public function store(Request $request){

        $request->validate([
            'title' => 'required',
            'image' => 'required',
        ]);  


        $image = $request->file('image');

        if ($image != null) {
            $new_name = rand() . '.' . $image->getClientOriginalExtension(); 
            $image->move(public_path('images/categories'), $new_name);
            $img_name = "/images/categories/".$new_name;
        }else{
            $img_name = "";
        }

        Categories::create($request->all());

        return redirect()->route('categories.index')
            ->with('success','Category created successfully.');
    }     

    public function show(Categories $categories){

        return view('categories.show',compact('categories'));
    } 

    public function edit(Categories $categories){

        return view('categories.edit',compact('categories'));
    }    

    public function update(Request $request, Categories $categories){

        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);    

        $categories->update($request->all());    

        return redirect()->route('categories.index')

            ->with('success','Categories updated successfully');
    }

    
    public function destroy(Categories $categories){

        $categories->delete();   
        return redirect()->route('categories.index')

            ->with('success','Categories deleted successfully');
    }

}
