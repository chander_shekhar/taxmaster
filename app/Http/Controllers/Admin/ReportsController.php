<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ReportsController extends Controller
{
    public function index()
    {
        $users = DB::select('SELECT * from users WHERE is_admin = 0 ORDER BY id DESC');    
        $cats  = DB::select('SELECT * from categories WHERE is_deleted = 0 ORDER BY id DESC');   

        return view( 'admin.reports' , [ 'users'=>$users, 'categories' => $cats ]  ); 
    }
}
