<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BanksController extends Controller
{
    public function index()
    {
        //$users = DB::select('SELECT * from users WHERE is_admin = 0 ORDER BY id DESC LIMIT 8');           
        // return view( 'admin.users',['users'=>$users] ); 
        return view( 'admin.banks' ); 
    }
}
