@extends('layouts.app')
 
@section('content')
 
@section('title', 'Varification Code')
   <div class="login-main verification">
      <div class="login-left">
         <div class="login-left-inn">
            <div class="logo-div">
               <a href=""><img src="{{ asset('images/logo.png') }}"></a>
            </div>
            <div class="login-right-inner">
               <h3>Verification Code</h3>
               <h4>Please enter your verification code<br> sent to  your mobile.</h4>
               <form>
                  <div class="form-group">
                     <input type="text" class="form-control" />
                     <input type="text" class="form-control" />
                     <input type="text" class="form-control" />
                     <input type="text" class="form-control" />
                  </div>
                  <div class="resend-div">If you don't recieve a code!
                     <a href="#" class="resend">Resend.</a>
                  </div>
                  <button type="submit" class="btn login">Verify</button>
               </form>
            </div>
         </div>
      </div>
      <div class="login-right">
         <img src="{{ asset('images/verification.png') }}" alt="">
      </div>
   </div>

@endsection
@section('scripts')

   <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
   <script type="text/javascript">
      
      $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
          if($currEl.val() === $("option:first", $currEl).val()) {
            $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
            $currEl.parent().removeClass('focused');
          } else {
            $('.control-label', $currEl.parent()).css({opacity: 1});
            $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
          }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
      }).trigger('blur');
   </script>
@endsection
