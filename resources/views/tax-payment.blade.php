@extends('layouts.app')
 
@section('content')
 
@section('title', 'Tax Payment')
        <div id="wrapper">
            @include('layouts.header')
           
            <div id="banner">
                <div class="container Tax_Payment">
                    <div class="left-banner-content">
                        <div class="total-profit">
                            <p>Total Profit/Loss</p>
                            <h2>$20.525,25</h2>
                            <button class="Pay_now">Pay Now</button>
                            <button class="View_details">View Details</button>
                        </div>
                    </div>
                    <div class="Transaction-history Personal_Information transaction_page">
                        <div class="container basic_info">
                            <ul class="transaction-list">
                                <li>
                                    <div class="right-tansction-detail">
                                        <h4>Payment Details</h4>
                                        <form>
                                            <div class="form-floating">
                                                <select class="form-select" id="floatingSelect" aria-label="Floating label select example">
                                                    <option selected>2020-2021</option>
                                                    <option value="1">2019-2020</option>
                                                    <option value="2">2018-2019</option>
                                                    <option value="3">2017-2018</option>
                                                </select>
                                                <label for="floatingSelect">Select Year</label>
                                            </div>
                                            <div class="form-group firstname">
                                                <label class="control-label" for="firstname">April - June</label>
                                                <input type="text" class="form-control" id="price" value="$1,200" />
                                            </div>
                                            <div class="form-group firstname last">
                                                <label class="control-label" for="lastname">Oct - Dec</label>
                                                <input type="text" class="form-control" id="price" value="$1,200"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="phone">July - Sept</label>
                                                <input type="text" class="form-control" id="price" value="$1,200"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="email">Jan - March</label>
                                                <input type="text" class="form-control" id="price" value="$1,200"/>
                                                <button class="pay-now" type="button">Pay Now</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- container -->
            </div>
             @include('layouts.footer')
        </div>

        @endsection

        @section('scripts')

            <script type="text/javascript">
                
                $('.form-control').on('focus blur change', function (e) {
                  var $currEl = $(this);
                  
                  if($currEl.is('select')) {
                    if($currEl.val() === $("option:first", $currEl).val()) {
                      $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
                      $currEl.parent().removeClass('focused');
                    } else {
                      $('.control-label', $currEl.parent()).css({opacity: 1});
                      $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
                    }
                  } else {
                    $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
                  }
                }).trigger('blur');
            </script>
            <script>
                $(document).ready(function(){
                  $(".dropbtn").click(function(){
                    $(".dropdown-content").toggleClass("show");
                  });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                  $('.menu-btn').click(function(event) {
                    $('.navbar-demo').toggleClass('open-nav');
                  });
                });
            </script>
        @endsection
