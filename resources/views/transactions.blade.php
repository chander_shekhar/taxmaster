@extends('layouts.app')
 
@section('content')

@section('title', 'Transactions')

<div id="wrapper">  

    @include('layouts.header')

    <link rel="stylesheet" href="{{ asset('css/range.css') }}">
    <link rel="stylesheet" href="{{ asset('css/semantic.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />

    <div class="main-section transaction-main-page">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="filter-div">
                        <h2><span class="filter-img"><img src="{{ asset('images/filter.png') }}"></span> Filter</h2>
                        <div class="filter-main-div">
                            <h3>Categories</h3>
                            <div class="categories-div">
                                <label class="categories-checkbox">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">Advertising Expenses</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Cart & Truck</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Repairs</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Commission & Fees</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Contact Labour</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark">Depletion</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Depreciation</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Insurance</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Emp Benefit Programs</span>
                                </label>
                                <label class="categories-checkbox">
                                <input type="checkbox">
                                <span class="checkmark">Mortgage Interest</span>
                                </label>
                            </div>
                            <!-- categories-div -->
                            <hr class="separator">
                            <h3>Date</h3>
                            <div class="categories-div datepicker-div">
                                <label class="container-radio">
                                <input type="radio" checked="checked" name="radio">
                                <span class="checkmark">Last Month</span>
                                </label>
                                <label class="container-radio">
                                <input type="radio"  name="radio">
                                <span class="checkmark">Last 3 Months</span>
                                </label>
                                <label class="container-radio">
                                <input type="radio"  name="radio">
                                <span class="checkmark">Last 6 Months</span>
                                </label>
                                <label class="container-radio">
                                <input type="radio"  name="radio">
                                <span class="checkmark">Last 1 Year</span>
                                </label>
                                <div class="ui form">
                                    <div class="two fields">
                                        <div class="field">
                                            <label>From:</label>
                                            <div class="ui calendar" id="rangestart">
                                                <div class="ui input left icon">
                                                    <input type="text" placeholder="Start">
                                                    <i class=" calendar fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label>To:</label>
                                            <div class="ui calendar" id="rangeend">
                                                <div class="ui input left icon">
                                                    <input type="text" placeholder="End">
                                                    <i class="fas fa-calendar-alt"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- categories-div -->
                            <hr class="separator">
                            <h3>Amount</h3>
                            <range-slider minValue="100" maxValue="1000" startMinPos="500" startMaxPos="700" um="$" ></range-slider>
                            <template id="range-slider">
                                <style type="text/css">
                                    .wrap {
                                    width: 100%;
                                    margin: 0 auto;
                                    float: left;
                                    }
                                    .slider-header-box {
                                    display: flex;
                                    justify-content: space-between;
                                    }
                                    .calculate-header-box-title {
                                    font-size: 18px;
                                    }
                                    .calculate-header-box-value {
                                    color: #ffffff;
                                    font-size: 18px;
                                    font-weight: 500;
                                    }
                                    .slider {
                                    margin-bottom: 20px;
                                    }
                                    .slider-slider {
                                    position: relative;
                                    margin-bottom: 5px;
                                    padding: 5px;
                                    }
                                    .slider-slider-item {
                                    display: block;
                                    width: 100%;
                                    height: 9px;
                                    margin-top: 10px;
                                    border-radius: 2px;
                                    margin-bottom: 10px;
                                    background: #869abe;
                                    position: relative;
                                    border-radius: 14px;
                                    }
                                    .slider-slider-touch {
                                    cursor: pointer;
                                    margin-top: -28px;
                                    position: absolute;
                                    z-index: 100;
                                    background: #4b6eaf;
                                    border: 5px solid #fff;
                                    border-radius: 100%;
                                    width: 15px;
                                    height: 15px;
                                    display: block;
                                    box-shadow: 0 0 2px 0 rgb(0 0 0 / 44%);
                                    }
                                    .money-value {
                                    float: left;
                                    width: 100%;
                                    margin-bottom: 30px;
                                    position: relative;
                                    } 
                                    .money-value span.money-item:last-child {
                                    margin: 0;
                                    }
                                    .money-value span.money-item {
                                    float: left;
                                    margin: 0;
                                    color: #fff;
                                    font-size: 16px;
                                    position: absolute;
                                    left: 0;
                                    }
                                    span.money-item.item2 {
                                    left: 29% !important;
                                    }
                                    span.money-item.item3 {
                                    left: 59%;
                                    }
                                    span.money-item.item4 {
                                    left: 90%;
                                    }
                                </style>
                                <div class="container">
                                    <div class="wrap">
                                        <div class="slider">
                                            <div class="slider-header-box">
                                                <span class="calculate-header-box-title">X</span>
                                                <span class="calculate-header-box-value" id="slider-value-min">0</span>
                                                <span class="calculate-header-box-value" id="slider-value-max">0</span>
                                            </div>
                                            <div class="slider-slider" id="slider-container">
                                                <span class="slider-slider-item" id="slider-item"></span>
                                                <span class="slider-slider-touch" data-value="min"></span>
                                                <span class="slider-slider-touch" data-value="max"></span>
                                            </div>
                                            <div class="money-value">
                                                <span class="money-item item1">$100</span>
                                                <span class="money-item item2">$500</span>
                                                <span class="money-item item3">$700</span>
                                                <span class="money-item item4">$1000</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </template>
                            <div class="categories-div date-div">
                                <label class="container-radio">
                                <input type="radio" checked="checked" name="radio">
                                <span class="checkmark">All</span>
                                </label>
                                <label class="container-radio">
                                <input type="radio" name="radio">
                                <span class="checkmark">Income</span>
                                </label>
                                <label class="container-radio">
                                <input type="radio" name="radio">
                                <span class="checkmark">Expenses</span>
                                </label>
                            </div>
                            <!-- categories-div -->
                            <div class="button-div">
                                <button type="button" class="btn reset" data-toggle="modal" data-target="#myModal">Reset</button>
                                <button type="button" class="btn reset cancel">Cancel</button>
                            </div>
                            <!-- button-div -->
                        </div>                        
                    </div>                    
                </div>               
                <div class="col-md-8">
                    <div class="right-side-filtered">
                        <div class="top-heading">
                            <a href="#" class="add-transaction" data-bs-toggle="modal" data-bs-target="#transaction-detail-edit">+</a>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Sort by A - Z</option>
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                            </select>
                            <a href="#" class="print"><img src="{{ asset('images/print.png') }}"></a>
                        </div>
                        <!-- top-heading -->
                        <ul class="transaction-list Transaction-page">
                            <li>
                                <a href="#"  data-bs-toggle="modal" data-bs-target="#transaction-detail">
                                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                    <div class="right-tansction-detail">
                                        <h3>Walmart</h3>
                                        <span>Jan 11:00 AM, 2021</span>
                                    </div>
                                    <div class="right-transaction-price">$200.00</div>
                                </a>
                            </li>
                            <li>
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Amazon</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>WinCo Food</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>D-Mart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Amazon</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>WinCo Food</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>D-Mart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                        </ul>
                    </div>
                    <!-- right-sde-filtered -->
                </div>
            </div>           
        </div>        
    </div>

    @include('layouts.footer')
</div>

<div class="modal fade" id="transaction-detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Transaction Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="transaction-detail-modal-main">
                    <h2>$20.<small>00$</small></h2>
                    <p>Transport allowance</p>
                    <h3 class="popup-heading">Categories</h3>
                    <div class="categories-div">
                        <label class="categories-checkbox">
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">Advertising Expenses</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Cart & Truck</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Repairs</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Commission & Fees</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Contact Labour</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">Depletion</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Depreciation</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Insurance</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Emp Benefit Programs</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Mortgage Interest</span>
                        </label>
                    </div>
                    <!-- categories-div -->
                    <h3 class="popup-heading">Detail</h3>
                    <ul class="transaction-inner-detail">
                        <li>
                            <h4>Payment Detail</h4>
                            <p>January 6th, 11:00 AM</p>
                        </li>
                        <li>
                            <h4>Ref</h4>
                            <p>Payment Invoice #2</p>
                        </li>
                        <li>
                            <h4>Amount</h4>
                            <p>$520.00</p>
                        </li>
                        <li>
                            <div class="file-upload">
                                <div class="file-upload-select">
                                    <div class="file-select-button" >Scan/Attach Receipts</div>
                                    <div class="file-select-name"></div>
                                    <input type="file" name="file-upload-input" id="file-upload-input">
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- transaction-inner-detail -->
                    <h3 class="popup-heading">Actions</h3>
                    <div class="addnote-div">
                        <h5>Add a note</h5>
                        <i class="fas fa-edit"></i>
                        <p>Add a note to transactions</p>
                        <div class="form-floating">
                            <select class="form-select" id="floatingSelect" aria-label="Floating label select example">
                                <option selected>Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                            <label for="floatingSelect">Assign Category</label>
                        </div>
                    </div>
                    <!-- addnote-div -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="transaction-detail-edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Transaction Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="transaction-detail-modal-main">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="floatingInput" placeholder="$ 20.00">
                        <label for="floatingInput">Amount</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="floatingInput1" placeholder="Transport allowance">
                        <label for="floatingInput1">Platform</label>
                    </div>
                    <h3 class="popup-heading">Categories</h3>
                    <div class="categories-div">
                        <label class="categories-checkbox">
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">Advertising Expenses</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Cart & Truck</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Repairs</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Commission & Fees</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Contact Labour</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox" checked="checked">
                        <span class="checkmark">Depletion</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Depreciation</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Insurance</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Emp Benefit Programs</span>
                        </label>
                        <label class="categories-checkbox">
                        <input type="checkbox">
                        <span class="checkmark">Mortgage Interest</span>
                        </label>
                    </div>
                    <!-- categories-div -->
                    <h3 class="popup-heading">Detail</h3>
                    <div class="date-picker-time">
                        <label for="date-time-picker1">Payment Date</label><br/>
                        <input id="date-time-picker1" class="jquery-date-time-picker" type="text" placeholder="15/07/2021, 11:00 AM">
                        <i class=" calendar fas fa-calendar-alt"></i>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="floatingInput1" placeholder="Payment Invoice #2">
                        <label for="floatingInput1">Ref</label>
                    </div>
                    <ul class="transaction-inner-detail">
                        <li>
                            <div class="file-upload">
                                <div class="file-upload-select">
                                    <div class="file-select-button" >Scan/Attach Receipts</div>
                                    <div class="file-select-name"></div>
                                    <input type="file" name="file-upload-input" id="file-upload-input">
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- transaction-inner-detail -->
                    <h3 class="popup-heading">Actions</h3>
                    <div class="addnote-div">
                        <h5>Add a note</h5>
                        <i class="fas fa-edit"></i>
                        <p>Add a note to transactions</p>
                    </div>
                </div>
                <!-- addnote-div -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

  <script src="{{ asset('js/semantic.js') }}" ></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>

  <script type="text/javascript">
      $('#rangestart').calendar({
        type: 'date',
        endCalendar: $('#rangeend')
      });
      $('#rangeend').calendar({
        type: 'date',
        startCalendar: $('#rangestart')
      });
      
      
      class RangeSlider extends HTMLElement {
        constructor() {
          super();
        };
        async connectedCallback() {
          this.minValue = parseInt( this.getAttribute( "minValue" ) );
          this.maxValue = parseInt( this.getAttribute( "maxValue" ) );
          this.startMinPos = this.getAttribute( "startMinPos" );
          this.startMaxPos = this.getAttribute( "startMaxPos" );
          this.um = this.getAttribute( "um" );
          this.root = this.attachShadow( { mode: 'open' } );
          this.root.appendChild( document.querySelector( '#range-slider' ).content.cloneNode( true ) );
          this.slider = this.root.getElementById( 'slider-container' );
          this.sliderClientCoords = this.slider.getBoundingClientRect();
          this.sliderWidth = this.slider.offsetWidth - this.root.querySelector( ".slider-slider-touch" ).offsetWidth / 2;
          this.actualPosMin = this.root.querySelector( ".slider-slider-touch[data-value='min']" );
          this.actualPosMax = this.root.querySelector( ".slider-slider-touch[data-value='max']" );
          this.cursorWidth = this.root.querySelector( ".slider-slider-touch" );
          this.root.querySelector( ".calculate-header-box-title" ).innerText = this.getAttribute( "title" );
          this.root.getElementById( 'slider-value-min' ).innerText = this.startMinPos + ' ' + this.um;
          this.root.getElementById( 'slider-value-max' ).innerText = this.startMaxPos + ' ' + this.um;
      
          const normalize = ( val, minVal, maxVal, newMin, newMax ) => {
            const scaled = ( ( ( val - minVal ) / ( maxVal - minVal ) ) * ( newMax - newMin ) ) + newMin;
            return scaled;
          };
      
          window.addEventListener( 'resize', () => { this.sliderClientCoords = this.slider.getBoundingClientRect() } );
          this.root.querySelector( ".slider-slider-touch[data-value='min']" ).style.left = normalize( this.startMinPos, this.minValue, this.maxValue, 0, this.sliderWidth ) + 'px';
          this.root.querySelector( ".slider-slider-touch[data-value='max']" ).style.left = normalize( this.startMaxPos, this.minValue, this.maxValue, 0, this.sliderWidth ) + 'px';
      
          const onStart = event => {
            this.item = event.currentTarget;
            this.sliderCoords = {};
            this.sliderCoords.top = this.sliderClientCoords.top + pageYOffset;
            this.sliderCoords.left = this.sliderClientCoords.left + pageXOffset;
            document.addEventListener( 'mouseup', onStop );
            document.addEventListener( 'touchend', onStop );
            document.addEventListener( 'mousemove', onMove );
            document.addEventListener( 'touchmove', onMove );
            const itemClientCoords = this.item.getBoundingClientRect();
            const itemCoords = {};
            itemCoords.top = itemClientCoords.top + pageYOffset;
            itemCoords.left = itemClientCoords.left + pageXOffset;
            this.shiftX = event.pageX - itemCoords.left;
          };
          const onMove = event => {
            let newLeft = event.pageX - this.sliderCoords.left - this.shiftX;
            switch( this.item.dataset.value ) {
              case 'min':
                if ( newLeft + this.item.offsetWidth > this.actualPosMax.offsetLeft ) {
                  return;
                };
                break;
              case 'max':
                if ( newLeft < this.actualPosMin.offsetLeft + this.item.offsetWidth ) {
                  return;
                };
                break;
            };
            if( newLeft < 0 ) newLeft = 0;
            if( newLeft > this.sliderWidth ) newLeft = this.sliderWidth;
            this.item.style.left = newLeft + 'px';
            this.root.getElementById( `slider-value-${this.item.dataset.value}` ).innerText = Math.round( normalize( newLeft, 0, this.sliderWidth, this.minValue, this.maxValue ) ) + ' ' + this.um;
          };
          const onStop = () => {
            document.removeEventListener( 'mousemove', onMove );
            document.removeEventListener( 'touchmove', onMove );
            document.removeEventListener( 'mouseup', onStop );
            document.removeEventListener( 'touchend', onStop );
          };
          this.root.querySelectorAll( ".slider-slider-touch" ).forEach( element => {
            element.addEventListener( 'mousedown', onStart );
            element.addEventListener( 'touchstart', onStart );
          });
        };
      };
      customElements.define( "range-slider", RangeSlider );
      
      
      let fileInput = document.getElementById("file-upload-input");
      let fileSelect = document.getElementsByClassName("file-upload-select")[0];
      fileSelect.onclick = function() {
        fileInput.click();
      }
      fileInput.onchange = function() {
        let filename = fileInput.files[0].name;
        let selectName = document.getElementsByClassName("file-select-name")[0];
        selectName.innerText = filename;
      }
      
  </script>
  <script>
      /*
        Ajax GET requests.
      */
      function ajaxJQueryGetRequest() {
            alert(Math.pow(10, 9));
            var selectedURL = "http://www.wxyz.abc";
            var datePicker1 = $("#date-time-picker1").val();
            var datePicker2 = $("#date-time-picker2").val();
            
            alert("Date1: " + datePicker1 + "\n" + , +
                  "Date2: " + datePicker2);
            
            // Check if dates are not empty
            if( datePicker1 && datePicker2 ) {
                var date1 = new Date(datePicker1);
                var date2 = new Date(datePicker2);                  
                 
                var datesData = JSON.stringify({
                      date1: date1.toUTCString(),
                      date2: date2.toUTCString()
                });
                  
                $.get(selectedURL, datesData).done(function(data) {
                      alert("Data Loaded: " + data);
                });  
            }            
      }
      
      $(document).ready(function() {
            $(".jquery-date-time-picker").datetimepicker();
      });
  </script>
  <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      });
  </script>
@endsection
