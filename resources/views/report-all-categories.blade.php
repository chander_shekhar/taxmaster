@extends('layouts.app')
 
@section('content')
 
@section('title', 'Report all Categories')

      <div id="wrapper">
         @include('layouts.header')

         
         <div class="Transaction-history Report_all_Categories">
            <div class="container">
               <ul class="transaction-list">
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>1.<span>Advertising Expense</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>2.<span>Car & Truck</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>3.<span>Commission & Fees</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>4.<span>Contract Labor</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>5.<span>Depreciation</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>6.<span>Emp Benefit Program</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>7.<span>Insurance</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>8.<span>Mortgage Interest</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>9.<span>Other Interest</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>10.<span>Legal Services</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>11.<span>Office Expense</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>12.<span>Pension & Profit</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>11.<span>Office Expense</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>12.<span>Pension & Profit</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>13.<span>Rent Machinery</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>14.<span>Rent Property</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>15.<span>Repairs</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>16.<span>Supplies</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>17.<span>Taxes & Licenses</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>18.<span>Travel</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>19.<span>Deductible Meals</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>20.<span>Utilities</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>21.<span>Wages</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>22.<span>Other Expenses</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>23.<span>Notice 2014-7.</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="lft_spc">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>24.<span>Total Expenses</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
                  <li class="expense">
                     <a href="Depreciation.html">
                        <div class="right-tansction-detail">
                           <h5>25.<span>Tentative PL</span></h5>
                        </div>
                        <div class="right-transaction-price">$200.00</div>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
          @include('layouts.footer')
      </div>

@endsection
@section('scripts')
      
  <script>
     $(document).ready(function(){
       $(".dropbtn").click(function(){
         $(".dropdown-content").toggleClass("show");
       });
     });
  </script>
  <script type="text/javascript">
     $(document).ready(function () {
       $('.menu-btn').click(function(event) {
         $('.navbar-demo').toggleClass('open-nav');
       });
     });
  </script>
  
@endsection
