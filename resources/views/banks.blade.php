@extends('layouts.app')
  
@section('content')

@section('title', 'Banks')  
<div id="wrapper">
 
    @include('layouts.header')  


    

    <div class="main-section">
        <div class="container">
            <div class="connected-banks">
                <h2>Connected Banks</h2>
                <button type="button" class="btn add-new-card" onclick="fetchLinkToken()"> + Add New Bank</button>
                <ul class="banks-list">

                    <?php 

                    //print_r($connected_banks);



                    ?>

                    <?php if( sizeof($connected_banks) > 0 ) {?>

                        @foreach ($connected_banks as $connected_bank)
                            <li>
                                <span class="bank-logo"><img src="{{ asset('images/bank3.png') }}"></span>
                                <div class="bank-detail">
                                    <h3>{{ $connected_bank->institution_name }}</h3>
                                    <p>XXXX-XXXX-XXXX-{{ $connected_bank->mask }}</p>
                                    <p class="price-bank">$20,222</p>
                                </div>
                               
                            </li>
                        @endforeach

                    <?php }else{ ?>

                        No Banks Yet.

                    <?php } ?>
                    <!-- <li>
                        <span class="bank-logo"><img src="{{ asset('images/bank2.png') }}"></span>
                        <div class="bank-detail">
                            <h3>U.S. Bancorp</h3>
                            <p>XXXX-XXXX-XXXX-2563</p>
                            <p class="price-bank">$20,222</p>
                        </div>
                       
                    </li>
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/bank1.png') }}"></span>
                        <div class="bank-detail">
                            <h3>Truist Financial Corporation</h3>
                            <p>XXXX-XXXX-XXXX-2563</p>
                            <p class="price-bank">$20,222</p>
                        </div>
                       
                    </li>
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/bank3.png') }}"></span>
                        <div class="bank-detail">
                            <h3>Bank of America Cop</h3>
                            <p>XXXX-XXXX-XXXX-2563</p>
                            <p class="price-bank">$20,222</p>
                        </div>
                       
                    </li> -->
                </ul>
                <!-- banks-list -->
            </div>
            <!--connected-banks -->
            <div class="connected-banks credit-cards">
                <h2>Credit Cards</h2>
                <button type="button" class="btn add-new-card"> + Add New Card</button>
                <ul class="banks-list card-list">
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/visa.png') }}"></span>
                        <div class="bank-detail">
                            <p>XXXX-XXXX-XXXX-2563</p>
                        </div>
                        <!-- bank-detail -->
                        <p class="price-bank">$20,222</p>
                    </li>
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/americaexpress.png') }}"></span>
                        <div class="bank-detail">
                            <p>XXXX-XXXX-XXXX-2563</p>
                        </div>
                        <!-- bank-detail -->
                        <p class="price-bank">$20,222</p>
                    </li>
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/mastercard.png') }}"></span>
                        <div class="bank-detail">
                            <p>XXXX-XXXX-XXXX-2563</p>
                        </div>
                        <!-- bank-detail -->
                        <p class="price-bank">$20,222</p>
                    </li>
                    <li>
                        <span class="bank-logo"><img src="{{ asset('images/discover.png') }}"></span>
                        <div class="bank-detail">
                            <p>XXXX-XXXX-XXXX-2563</p>
                        </div>
                        <!-- bank-detail -->
                        <p class="price-bank">$20,222</p>
                    </li>
                </ul>
                <!-- banks-list -->
            </div>
            <!--connected-banks -->
        </div>
        <!-- container -->
    </div>

    <!-- Please link your <strong>primary bank account</strong>.
    <button id="linkButton">Link Your Bank Accounts</button> -->


    <script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>

    <?php

        $plaid_env          = 'sandbox';
        $plaid_client_id    = env('PLAID_CLIENT_ID');
        $plaid_public       = env('PLAID_DEVELOPMENT_ID');
        $plaid_secret       = env('PLAID_SANDBOX_ID');
        $plaid_url          = "https://sandbox.plaid.com";
    ?>

    @include('layouts.footer')
</div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function(){
          $(".dropbtn").click(function(){
            $(".dropdown-content").toggleClass("show");
          });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
          $('.menu-btn').click(function(event) {
            $('.navbar-demo').toggleClass('open-nav');
          });
        });   

    </script>

    <script type="text/javascript">
        // (async function() {
        //     const fetchLinkToken = async () => {
        //         // const response = await fetch('/create_link_token', { method: 'POST', data : {"_token":"{{ csrf_token() }}"} });

        //         const response = await fetch("/create_link_token", {
        //             headers: {
        //                 "Content-Type"      : "application/json",
        //                 "Accept"            : "application/json",
        //                 "X-Requested-With"  : "XMLHttpRequest",
        //                 "X-CSRF-Token"      : '{{ csrf_token() }}'
        //             },
        //             method                  : "POST",
        //             credentials             : "same-origin",
        //             body                    : JSON.stringify({
        //                 _token : '{{ csrf_token() }}'
        //             })
        //         })

        //         const responseJSON = await response.json();
        //         //console.log(responseJSON);
        //         return responseJSON.public_token;
        //     };

            
        //     const configs = {                
        //         token: await fetchLinkToken(),
        //         onSuccess: async function(public_token, metadata) {                    

        //             console.log(public_token);
        //             console.log(metadata);
        //             await fetch("/exchange_public_token", {
        //                 headers: {
        //                     "Content-Type"      : "application/json",
        //                     "Accept"            : "application/json",
        //                     "X-Requested-With"  : "XMLHttpRequest",
        //                     "X-CSRF-Token"      : '{{ csrf_token() }}'
        //                 },
        //                 method                  : "POST",
        //                 credentials             : "same-origin",
        //                 body                    : JSON.stringify({
        //                     _token              : '{{ csrf_token() }}',
        //                     public_token        : public_token
        //                 })
        //             })
        //         },
        //         onExit: async function(err, metadata) {                   
        //             if (err != null && err.error_code === 'INVALID_LINK_TOKEN') {
        //                 linkHandler.destroy();
        //                 linkHandler = Plaid.create({
        //                     ...configs,
        //                     token: await fetchLinkToken(),
        //                 });
        //             }
        //             if (err != null) {                       
        //                 console.log(err)
        //             }                    
        //         },
        //     };

        //     //var linkHandler = Plaid.create(configs);
        //     document.getElementById('linkButton').onclick = function() {
        //         linkHandler.open();
        //     };

        //     console.log("keyyy  =>   ",'139a10c5540a60164b7dc2321a626f')

        //     var linkHandler     = Plaid.create({

        //         selectAccount   : true,
        //         env             : 'sandbox',
        //         apiVersion      : 'v2',
        //         clientName      : 'Client Name',
        //         key             : '139a10c5540a60164b7dc2321a626f',
        //         product         : ['auth'],
        //         selectAccount   : true,
        //         onLoad          : function() {
                  
        //         },
        //         token: await fetchLinkToken(),                
        //         onSuccess: async function(public_token, metadata) {                    

        //             console.log(public_token);
        //             console.log(metadata);
        //             await fetch("/exchange_public_token", {
        //                 headers: {
        //                     "Content-Type"      : "application/json",
        //                     "Accept"            : "application/json",
        //                     "X-Requested-With"  : "XMLHttpRequest",
        //                     "X-CSRF-Token"      : '{{ csrf_token() }}'
        //                 },
        //                 method                  : "POST",
        //                 credentials             : "same-origin",
        //                 body                    : JSON.stringify({
        //                     _token              : '{{ csrf_token() }}',
        //                     public_token        : public_token
        //                 })
        //             })
        //         },              
        //         onExit: async function(err, metadata) {
                    
        //             if (err != null) {
        //                 // Handle any other types of errors.
        //             }
        //             console.log('metadata =>',  metadata);
        //         },
        //     });

        // })();
        
    </script>

    <!-- <input type="hidden" id="linkToken" value=""><br /> -->
    <input type="hidden" id="linkToken" ><br />
    <!-- <button type="button" onclick="fetchLinkToken()">Connect Plaid Account</button> -->
    <!-- <button type="button" class="balance_btn" onclick="fetchBalance()" style="display: none;">Fetch Balance</button> -->

    <script>


        const fetchLinkToken = async () => {            
            const response = await fetch("/create_link_token_v1", {
                headers: {
                    "Content-Type"      : "application/json",
                    "Accept"            : "application/json",
                    "X-Requested-With"  : "XMLHttpRequest",
                    "X-CSRF-Token"      : '{{ csrf_token() }}'
                },
                method                  : "POST",
                credentials             : "same-origin",
                body                    : JSON.stringify({
                    _token : '{{ csrf_token() }}'
                })
            })
            const responseJSON = await response.json();
            $('#linkToken').val(responseJSON.link_token);        
            launchPlaid();   
        };

        const exchange_public_token = async (public_token,metadata ) => {            
            const response = await fetch("/exchange_public_token", {
                headers: {
                    "Content-Type"      : "application/json",
                    "Accept"            : "application/json",
                    "X-Requested-With"  : "XMLHttpRequest",
                    "X-CSRF-Token"      : '{{ csrf_token() }}'
                },
                method                  : "POST",
                credentials             : "same-origin",
                body                    : JSON.stringify({

                    _token          : '{{ csrf_token() }}',
                    public_token    : public_token,
                    accounts        : metadata.accounts,
                    institution     : metadata.institution,
                    link_session_id : metadata.link_session_id
                })
            })
            //const responseJSON = await response.json();            
            //console.log(responseJSON);
            //$('.balance_btn').show();

            location.reload();
        };


        function launchPlaid() {

            console.log( $('#linkToken').val() )

            const handler = Plaid.create({
                token: document.getElementById('linkToken').value,
                    // The onSuccess() function of the Plaid plugin will return a public
                    // token and metadata object to your code. Please use our API to send
                    // us the public token, and we will query the routing number and account
                    // number of the bank account from Plaid and store it securely on our systems.
                onSuccess: (public_token, metadata) => { 
                    document.getElementById('plaidToken').innerHTML = public_token;
                    document.getElementById('plaidMetadata2').value = JSON.stringify(metadata, null, 2); 

                    exchange_public_token(public_token, metadata);

                },
                onLoad: () => { },
                onExit: (err, metadata) => { 
                  document.getElementById('plaidToken').innerHTML = err;
                  document.getElementById('plaidMetadata2').value = JSON.stringify(metadata, null, 2); 
                },
                onEvent: (eventName, metadata) => { },
                receivedRedirectUri: null,
            });
                // Calling open() will display the "Institution Select" view to your user, 
                // starting the Link flow. 
            handler.open();
        }

    </script>
    <p id='plaidToken' style="display: none;"></p><BR>
    <textarea id='plaidMetadata2' rows='30' cols='50' style="display: none"></textarea>



@endsection