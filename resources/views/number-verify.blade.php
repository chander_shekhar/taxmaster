@extends('layouts.app')
 
@section('content')
 
@section('title', 'Verify')

   <div class="login-main forgot-page">
      <div class="login-left">
         <div class="login-left-inn">
            <div class="logo-div">
               <a href=""><img src="{{ asset('images/logo.png') }}"></a>
            </div>
            <div class="login-right-inner">


               <div class="alert alert-danger" id="error" style="display: none;"></div>
               <div class="alert alert-success" id="successAuth" style="display: none;"></div>
               <h3>Number Verify</h3>
               <h4>Please enter your phone along with<br> country code</h4>
               <form>
                  <div class="form-group">
                     <label class="control-label" for="number">Phone Number</label>
                     <input type="text" id="number" class="form-control">
                  </div>

                  <div id="recaptcha-container"></div>


                  <button type="button" class="btn btn-primary mt-3" onclick="sendOTP();">Send OTP</button>
               </form>

               <div class="mb-5 mt-5">
                  <h3>Add verification code</h3>

                  <div class="alert alert-success" id="successOtpAuth" style="display: none;"></div>

                  <form>
                      <input type="text" id="verification" class="form-control" placeholder="Verification code">
                      <button type="button" class="btn btn-danger mt-3" onclick="verify()">Verify code</button>
                  </form>
              </div>


            </div>
         </div>
      </div>
      <div class="login-right">
         <img src="{{ asset('images/verification.png') }}" alt="">
      </div>
   </div>

@endsection
@section('scripts')
   <!-- <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script> -->

   <script>
      
      var firebaseConfig = {
        apiKey: "AIzaSyBOO5Gm4m4SIfgH0Qs9lr_FZ4V71mEmJsI",
        authDomain: "taxmaster-001.firebaseapp.com",
        projectId: "taxmaster-001",
        storageBucket: "taxmaster-001.appspot.com",
        messagingSenderId: "764573004775",
        appId: "1:764573004775:web:d10a1bff71f4139d468342",
        measurementId: "G-PMGE8BXRKG"
      };
      
      firebase.initializeApp(firebaseConfig);

    </script>

    <script type="text/javascript">
        window.onload = function () {
            render();
        };

        function render() {
           // window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
           // recaptchaVerifier.render();
           window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
               'size': 'invisible',
               'callback': (response) => {
                   // reCAPTCHA solved, allow signInWithPhoneNumber.
                   // onSignInSubmit();
                   // $('.enable_on_captcha').prop('disabled',false)
                   console.log("yAyy")

               }
           });
       }

       function sendOTP() {
           var number = $("#number").val();
           firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
               window.confirmationResult = confirmationResult;
               coderesult = confirmationResult;
               console.log(coderesult);
               $("#successAuth").text("Message sent");
               $("#successAuth").show();
           }).catch(function (error) {
               $("#error").text(error.message);
               $("#error").show();
           });
       }

       function verify() {
           var code = $("#verification").val();
           coderesult.confirm(code).then(function (result) {
               var user = result.user;
               console.log(user);
               $("#successOtpAuth").text("Auth is successful");
               $("#successOtpAuth").show();
           }).catch(function (error) {
               $("#error").text(error.message);
               $("#error").show();
           });
       }
    </script>


   <script type="text/javascript">
      
      $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
          if($currEl.val() === $("option:first", $currEl).val()) {
            $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
            $currEl.parent().removeClass('focused');
          } else {
            $('.control-label', $currEl.parent()).css({opacity: 1});
            $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
          }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
      }).trigger('blur');
   </script>
@endsection