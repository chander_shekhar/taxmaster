@extends('layouts.appAdmin')
 
@section('content')

@section('title', 'Dashboard') 

   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin', array('page'=>'transaction') )  

         
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
         <link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.css') }}">
         
         <div class="right-main">
            <div class="right-main-inn dashboard-main-page">
               <div class="dasboard-page-top">
                  <div class="row">
                     <div class="col-md-7">
                        <div class="dashboard-graph">

                           <?php      


                              $users_data = array(
                                 array("label"=> "Sunday", "y"=> 0 ),
                                 array("label"=> "Monday", "y"=> 0 ),
                                 array("label"=> "Tuesday", "y"=> 0 ),
                                 array("label"=> "Wednesday", "y"=> 0 ),
                                 array("label"=> "Thursday", "y"=> 0 ),
                                 array("label"=> "Friday", "y"=> 0 ),
                                 array("label"=> "Saturday", "y"=> 0 )
                              );

                              $banks_data = array(
                                 array("label"=> "Sunday", "y"=> 0 ),
                                 array("label"=> "Monday", "y"=> 0 ),
                                 array("label"=> "Tuesday", "y"=> 0 ),
                                 array("label"=> "Wednesday", "y"=> 0 ),
                                 array("label"=> "Thursday", "y"=> 0 ),
                                 array("label"=> "Friday", "y"=> 0 ),
                                 array("label"=> "Saturday", "y"=> 0 )
                              );

                              if ($users[0]->total_users > 0 ){

                                 $users_data = array(
                                    array(   "label"=> "Sunday",     "y"=> $users[0]->Sunday ),
                                    array(   "label"=> "Monday",     "y"=> $users[0]->Monday ),
                                    array(   "label"=> "Tuesday",    "y"=> $users[0]->Tuesday ),
                                    array(   "label"=> "Wednesday",  "y"=> $users[0]->Wednesday ),
                                    array(   "label"=> "Thursday",   "y"=> $users[0]->Thursday ),
                                    array(   "label"=> "Friday",     "y"=> $users[0]->Friday ),
                                    array(   "label"=> "Saturday",   "y"=> $users[0]->Saturday )
                                 );

                              }
                               
                              if ($banks[0]->total_users > 0 ){

                                 $banks_data = array(
                                    array(   "label"=> "Sunday",     "y"=> $banks[0]->Sunday ),
                                    array(   "label"=> "Monday",     "y"=> $banks[0]->Monday ),
                                    array(   "label"=> "Tuesday",    "y"=> $banks[0]->Tuesday ),
                                    array(   "label"=> "Wednesday",  "y"=> $banks[0]->Wednesday ),
                                    array(   "label"=> "Thursday",   "y"=> $banks[0]->Thursday ),
                                    array(   "label"=> "Friday",     "y"=> $banks[0]->Friday ),
                                    array(   "label"=> "Saturday",   "y"=> $banks[0]->Saturday )
                                 );

                              }
                               
                           ?>

                           <!-- <ul class="graph-series">
                              <li><span class="users"></span> Users</li>
                              <li><span class="banks-span"></span> Banks</li>
                           </ul> -->
                           <select class="graph-select">
                              <option value='<?php echo url('admin/dashboard?filter=week') ?>'>Last Week</option>
                              <!-- <option>Last Month</option> -->
                              <!-- <option>Last Quarter</option> -->
                              <option value='<?php echo url('admin/dashboard?filter=year') ?>'>Last Year</option>
                           </select>
                           <div class="graph-main-div">
                              <!-- <img src="{{ asset('images/admin/dashboar-graph.png') }}"> -->

                              <div id="chartContainer" style="height: 370px; width: 100%;"></div>

                           </div>
                          
                        </div>
                        
                     </div>
                     
                     <div class="col-md-5">
                        <div class="dashboard-bank-list">
                           <h3>Recent Banks</h3>
                           <a href="<?=env('APP_URL')?>banks" class="see-banks">See All</a>
                           <div class="content-fullpage mCustomScrollbar">
                              <ul class="banks-list-recent">                                 


                                 <?php if( sizeof($banks_latest) > 0 ) {?>

                                    @foreach ($banks_latest as $bank)                                      

                                       <li>
                                          <span class="bank-logo"><img src="{{asset($bank->image)}}"></span>
                                          <div class="recentbank-data">
                                             <h4>{{ $bank->name }}</h4>
                                             <span>XXXX-XXXX-XXXX-5236</span>
                                             <p>$ 20,522</p>
                                          </div>
                                       </li>

                                    @endforeach

                                 <?php }else{ ?>

                                    No Banks Yet.

                                 <?php } ?>


                                 <!-- <li>
                                    <span class="bank-logo"><img src="{{ asset('images/admin/bank_of_america.png') }}"></span>
                                    <div class="recentbank-data">
                                       <h4>Bank of America Corp</h4>
                                       <span>XXXX-XXXX-XXXX-5236</span>
                                       <p>$ 20,522</p>
                                    </div>
                                 </li>
                                 <li>
                                    <span class="bank-logo"><img src="{{ asset('images/admin/truist.png') }}"></span>
                                    <div class="recentbank-data">
                                       <h4>Truist Financial Corporation</h4>
                                       <span>XXXX-XXXX-XXXX-5236</span>
                                       <p>$ 20,522</p>
                                    </div>
                                 </li>
                                 <li>
                                    <span class="bank-logo"><img src="{{ asset('images/admin/us_bancorp.png') }}"></span>
                                    <div class="recentbank-data">
                                       <h4>U.S. Bancorp</h4>
                                       <span>XXXX-XXXX-XXXX-5236</span>
                                       <p>$ 20,522</p>
                                    </div>
                                 </li>
                                 <li>
                                    <span class="bank-logo"><img src="{{ asset('images/admin/bank_of_america.png') }}"></span>
                                    <div class="recentbank-data">
                                       <h4>Bank of America Corp</h4>
                                       <span>XXXX-XXXX-XXXX-5236</span>
                                       <p>$ 20,522</p>
                                    </div>
                                 </li> -->
                              </ul>
                              <!-- banks-list -->
                           </div>
                        </div>
                        <!-- dashboard-bank-list -->
                     </div>
                     <!-- col-md-5 -->
                  </div>
               </div>
               <!-- dasboard-page-top -->
               <div class="dashboard-main-div posting-page">
                  <h2 class="dashboard-btm-heading">Recent Users</h2>
                  <div class="dashbord-main-inner">
                     <div class="content-fullpage mCustomScrollbar">
                        <div class="table-responsive">
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Tax Payer</th>
                                    <th scope="col">Web File</th>
                                    <th scope="col">EIN</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 <?php if( sizeof($users_latest) > 0 ) {?>

                                    @foreach ($users_latest as $user)
                                       <tr>
                                          <td>{{ $user->first_name }}</td>
                                          <td>{{ $user->last_name }}</td>
                                          <td>{{ $user->email }}</td>
                                          <td>{{ $user->phone }}</td>
                                          <td>{{ $user->tax_payer }}</td>
                                          <td>{{ $user->webfile }}</td>
                                          <td>{{ $user->ein }}</td>
                                       </tr>
                                    @endforeach

                                 <?php }else{ ?>

                                    No Users Yet.

                                 <?php } ?>
                                 
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- dashboard-main-inner -->
               </div>
               <!-- dashboard-main-div -->
            </div>
         </div>
      </div>
   </div>
     
@endsection

@section('scripts')


   <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
   <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
      
   <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

   <script type="text/javascript">
      $(function () {
         $('select').selectpicker();      

      });


      $(".graph-select").change(function(){
          document.location.href = $(this).val();
      });

      window.onload = function () {
 
         var chart = new CanvasJS.Chart("chartContainer", { 
            theme: "light2",
            title: {
               text: "Users and Banks"
            },
            subtitles: [{
               text: "In numbers"
            }],
            legend:{
               cursor: "pointer",
               itemclick: toggleDataSeries
            },
            toolTip: {
               shared: true
            },
            data: [
            {
               type: "stackedArea",
               name: "Users",
               showInLegend: true,
               visible: false,
               // yValueFormatString: "#,##0 GWh",
               dataPoints: <?php echo json_encode($users_data, JSON_NUMERIC_CHECK); ?>
            },
            {
               type: "stackedArea",
               name: "Banks",
               showInLegend: true,
               // yValueFormatString: "#,##0 GWh",
               dataPoints: <?php echo json_encode($banks_data, JSON_NUMERIC_CHECK); ?>
            }]
         });
 
         chart.render();
 
         function toggleDataSeries(e){
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
               e.dataSeries.visible = false;
            }
            else{
               e.dataSeries.visible = true;
            }
            chart.render();
         }
 
      }

   </script>

@endsection
