@extends('layouts.appAdmin')
  
@section('content') 

@section('title', 'Users')
    
   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin')
         
         <div class="right-main">
            <div class="right-main-inn workout-plan">
               <div class="dashboard-top-div">
                  <div class="dashboard-right-top-div">
                     <div class="search-div">
                        <form action="<?=url('admin/users')?>" method="GET" autocomplete="off">
                           <input type="text" class="input-control search-input" placeholder="Search" name="search">
                           <button type="submit" class="btn btn-default-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                  </div>
                  
               </div>
               
               <div class="dashboard-main-div posting-page">
                  <div class="dashbord-main-inner">
                     <div class="content-fullpage mCustomScrollbar">
                        <div class="table-responsive">
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Tax Payer</th>
                                    <th scope="col">Web File</th>
                                    <th scope="col">EIN</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 <?php if( sizeof($users) > 0 ) {?>

                                    @foreach ($users as $user)
                                       <tr>
                                          <td>{{ $user->first_name }}</td>
                                          <td>{{ $user->last_name }}</td>
                                          <td>{{ $user->email }}</td>
                                          <td>{{ $user->phone }}</td>
                                          <td>{{ $user->tax_payer }}</td>
                                          <td>{{ $user->webfile }}</td>
                                          <td>{{ $user->ein }}</td>
                                       </tr>
                                    @endforeach

                                 <?php }else{ ?>

                                    No Users Yet.

                                 <?php } ?>
                                                               
                              </tbody>
                           </table>

                           <div class="d-flex justify-content-center">
                              {!! $users->links() !!}
                           </div>


                        </div>
                     </div>
                  </div>
                  
               </div>
               
            </div>
         </div>
      </div>
   </div>
      
   <div id="delete-user" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <div class="modal-body-content">
                  <span class="dlt-img"><img src="images/done.png" alt=""></span>
                  <h4>Done!</h4>
                  <p>Workout Partner deleted successfully</p>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
               </div>
               <!-- modal-body-content -->
            </div>
         </div>
      </div>
   </div>
      
@endsection

@section('scripts')

   <script type="text/javascript">
        
   </script>

@endsection
