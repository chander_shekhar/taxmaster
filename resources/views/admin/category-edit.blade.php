@extends('layouts.appAdmin')
  
@section('content') 
 
@section('title', 'Categories')  
      
   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin')
      
         <div class="right-main">
           <div class="right-main-inn workout-plan">
                        

              <div class="dashboard-main-div posting-page">
                 <div class="dashbord-main-inner">
                    <div class="content-fullpage mCustomScrollbar">
                       <div class="table-responsive">

                           <h4>Edit Category</h4>


                           @if ($message = Session::get('success'))
                              <div class="alert alert-success">
                                 <p>{{ $message }}</p>
                              </div>
                           @endif

                           @if ($message = Session::get('warning'))
                              <div class="alert alert-warning">
                                 <p>{{ $message }}</p>
                              </div>
                           @endif


                           @if ($errors->any())
                               <div class="alert alert-danger">
                                   <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                   <ul>
                                       @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                       @endforeach
                                   </ul>
                               </div>
                           @endif



                           <form action="{{ route('admin.categories.update', $category[0]->id ) }}" method="POST" enctype="multipart/form-data" autocomplete="off">

                              @csrf                          
                              @method('PUT')

                              <div class="user_image_circle">                        
                                 <div class="file-input flex items-center">                            
                                    <div class="h-12 w-12 rounded-full overflow-hidden bg-gray-100">                           
                                                    
                                       <div x-show="previewPhoto" class="h-12 w-12 rounded-full overflow-hidden">                              
                                          <img src="{{asset($category[0]->image)}}" id="user_p_img" class="h-12 w-12 object-cover" height="50" width="50">                                 
                                       </div>
                                    </div>
                                    <div class="flex items-center edit icon">                            
                                       <div class="ml-5 rounded-md shadow-sm">                            
                                          <input type="file" 
                                                 accept="image/*,capture=camera" 
                                                 name="image" id="photo" 
                                                 class="custom">
                                          <label for="photo" class="edit-image">
                                             <img src="{{ asset('images/edit.png') }}">
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="form-group firstname">
                                 <label class="control-label" for="firstname">Category Name</label>
                                 <input type="text" class="form-control" id="catname" name="title" value="{{ $category[0]->title }}" required />
                              </div>          
                              <input type="hidden" name="id" value="{{$category[0]->id}}">
                              <button type="submit" class="btn btn-primary">Edit Category</button>
                              
                           </form>                           
                       </div>
                    </div>
                 </div>                 
              </div>
           </div>
         </div>
      </div>
   </div>

@endsection

@section('scripts')  
   
     
   <script type="text/javascript">

      photo.onchange = evt => {
         const [file] = photo.files
         if (file) {
            user_p_img.src = URL.createObjectURL(file)
         }
      }
     
       
   </script>
@endsection
