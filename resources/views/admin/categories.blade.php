@extends('layouts.appAdmin')
  
@section('content') 

@section('title', 'Categories')  
      
   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin')
      
         <div class="right-main">
           <div class="right-main-inn workout-plan">
              <div class="dashboard-top-div">
                 <div class="dashboard-right-top-div">

                    <div class="search-div">
                      <form action="{{ route('admin.categories.index') }}" method="GET" autocomplete="off">
                          <input type="text" class="input-control search-input" placeholder="Search" name="search">
                          <button type="submit" class="btn btn-default-sm"><i class="fa fa-search" aria-hidden="true"></i></button>
                      </form>
                    </div>
                    
                    <a href="#" class="btn filter" data-toggle="modal" data-target="#create-category">+ Add Category</a>

                 </div>
              </div>             

              <div class="dashboard-main-div posting-page">
                 <div class="dashbord-main-inner">
                    <div class="content-fullpage mCustomScrollbar">
                       <div class="table-responsive">


                           @if ($message = Session::get('success'))
                              <div class="alert alert-success">
                                 <p>{{ $message }}</p>
                              </div>
                           @endif

                           @if ($message = Session::get('warning'))
                              <div class="alert alert-warning">
                                 <p>{{ $message }}</p>
                              </div>
                           @endif


                           @if ($errors->any())
                               <div class="alert alert-danger">
                                   <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                   <ul>
                                       @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                       @endforeach
                                   </ul>
                               </div>
                           @endif
 
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Category Name</th>
                                 </tr>
                              </thead>
                              <tbody>

                                 <?php if(sizeof($categories) > 0) { ?>

                                    @foreach ($categories as $category)
                                       <tr>
                                          <td>
                                            <div class="video-div"><img src="{{asset($category->image)}}"></div>
                                          </td>
                                          <td>{{ $category->title }}</td>
                                          <td>

                                             <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST">

                                                <!-- <a href="Meal-detail.html" class="edit view"><img src="images/view.png"></a> -->
                                                <a href="{{ route('admin.categories.edit',$category->id) }}" class="edit"><img src="{{ asset('images/admin/edit-icon.png') }}"></a>


                                                @csrf

                                                @method('DELETE')


                                                <!-- <a href="#" class="edit trash" data-toggle="modal" data-target="#delete-user"><img src="{{ asset('images/admin/trash-icon.png') }}"></a> -->

                                                <input type="hidden" name="id" value="{{ $category->id }}">
                                                <button type="submit" class="btn btn-danger">Delete</button>

                                             </form>
                                          </td>
                                       </tr>
                                    @endforeach
                                 <?php } else { ?>

                                    <tr>

                                      <td>  No Categories Yet.</td>
                                    </tr>

                                 <?php  } ?>
                                
                              </tbody>
                           </table>

                           <div class="d-flex justify-content-center">
                              {!! $categories->links() !!}
                          </div>


                       </div>
                    </div>
                 </div>                 
              </div>
           </div>
         </div>
      </div>
   </div>

   <div id="create-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createCategory" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;  </button>
               <h4 class="modal-title" id="createCategory">Create Category</h4>
            </div>

            <form action="{{ route('admin.categories.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">

               @csrf

               <div class="modal-body">

                  <div class="user_image_circle">                        
                     <div class="file-input flex items-center">                            
                        <div class="h-12 w-12 rounded-full overflow-hidden bg-gray-100">                           
                                        
                           <div x-show="previewPhoto" class="h-12 w-12 rounded-full overflow-hidden">                              
                              <img :src="" id="user_p_img" class="h-12 w-12 object-cover" height="50" width="50">                                 
                           </div>
                        </div>
                        <div class="flex items-center edit icon">                            
                           <div class="ml-5 rounded-md shadow-sm">                            
                              <input type="file" 
                                     accept="image/*,capture=camera" 
                                     name="image" id="photo" 
                                     class="custom">
                              <label for="photo" class="edit-image">
                                 <img src="{{ asset('images/edit.png') }}">
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="form-group firstname">
                     <label class="control-label" for="firstname">Category Name</label>
                     <input type="text" class="form-control" id="catname" name="title" value="" required />
                  </div>              
                 
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Add Category</button>
               </div>
            </form>
         </div>
      </div>
   </div>

@endsection

@section('scripts')  
   
     
   <script type="text/javascript">

      photo.onchange = evt => {
         const [file] = photo.files
         if (file) {
            user_p_img.src = URL.createObjectURL(file)
         }
      }
     
       
   </script>
@endsection
