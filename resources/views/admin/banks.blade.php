@extends('layouts.appAdmin')
 
@section('content')

@section('title', 'Banks')
   <div class="container-fluid">
      <div class="row">

          @include('layouts.headerAdmin')
         
         <div class="right-main">
            <div class="right-main-inn workout-plan">
               <div class="dashboard-top-div">
                  <div class="dashboard-right-top-div">
                     <div class="search-div"><input type="text" class="input-control search-input" placeholder="Search" name=""><i class="fa fa-search" aria-hidden="true"></i></div>
                  </div>                  
               </div>
               
               <div class="dashboard-main-div posting-page">
                  <div class="dashbord-main-inner">
                     <div class="content-fullpage mCustomScrollbar">
                        <div class="table-responsive">
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Account Number</th>
                                    <th scope="col">User Name</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/bank_of_america.png') }}"></div>
                                    </td>
                                    <td>Bank of America Corp.</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>John Smith</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/us_bancorp.png') }}"></div>
                                    </td>
                                    <td>U.S. Bancorp</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Emily Waltson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/truist.png') }}"></div>
                                    </td>
                                    <td>Truist Financial Corporation</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Anee Watson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/bank_of_america.png') }}"></div>
                                    </td>
                                    <td>Bank of America Corp.</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>John Smith</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/us_bancorp.png') }}"></div>
                                    </td>
                                    <td>U.S. Bancorp</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Emily Waltson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/truist.png') }}"></div>
                                    </td>
                                    <td>Truist Financial Corporation</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Anee Watson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/bank_of_america.png') }}"></div>
                                    </td>
                                    <td>Bank of America Corp.</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>John Smith</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/us_bancorp.png') }}"></div>
                                    </td>
                                    <td>U.S. Bancorp</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Emily Waltson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/truist.png') }}"></div>
                                    </td>
                                    <td>Truist Financial Corporation</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Anee Watson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/bank_of_america.png') }}"></div>
                                    </td>
                                    <td>Bank of America Corp.</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>John Smith</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/us_bancorp.png') }}"></div>
                                    </td>
                                    <td>U.S. Bancorp</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Emily Waltson</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="video-div"><img src="{{ asset('images/admin/truist.png') }}"></div>
                                    </td>
                                    <td>Truist Financial Corporation</td>
                                    <td>XXXX-XXXX-XXXX-5896</td>
                                    <td>Anee Watson</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- dashboard-main-inner -->
               </div>
               
            </div>
         </div>
      </div>
   </div>

   <div id="delete-user" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
               <div class="modal-body-content">
                  <span class="dlt-img"><img src="{{ asset('images/admin/done.png') }}" alt=""></span>
                  <h4>Done!</h4>
                  <p>Workout Partner deleted successfully</p>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
               </div>
               <!-- modal-body-content -->
            </div>
         </div>
      </div>
   </div>

@endsection

@section('scripts')

   <script type="text/javascript">
       
   </script>

@endsection