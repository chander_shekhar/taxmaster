@extends('layouts.appAdmin')
 
@section('content')

@section('title', 'Transactions')    

   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin')         
         <!-- <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}"> -->
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
         <link rel="stylesheet" href="{{ asset('css/bootstrap-clockpicker.min.css') }}">
         <link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.css') }}">     
         <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />         
         <div class="right-main">
            <div class="right-main-inn workout-plan Transactions-page-div">
               <div class="dashboard-top-div">
                  <div class="transaction-page-filter">
                     <div class="form-group">

                        <label class="control-label" for="selectCtrl">Users</label>

                        <?php if( sizeof($users) > 0 ) {?>
                           <select type="text" class="form-control" id="user_sel">

                              @foreach ($users as $user)
                                 <tr>
                                    <option value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</option>

                                 </tr>
                              @endforeach

                           </select>

                        <?php }else{ ?>   

                           <select type="text" class="form-control" id="user_sel">                           

                              <option disabled>No Users Yet.</option>

                           </select>

                        <?php } ?>

                     </div>
                     <div class="form-group">
                        <label class="control-label" for="selectCtrl">Categories</label>


                        <?php if( sizeof($categories) > 0 ) {?>
                           <select type="text" class="form-control" id="cat_sel">

                              @foreach ($categories as $cat)
                                 <tr>
                                    <option value="{{ $cat->id }}">{{ $cat->title }}</option>

                                 </tr>
                              @endforeach

                           </select>

                        <?php }else{ ?>   

                           <select type="text" class="form-control" id="user_sel">                           

                              <option disabled>No Categories Yet.</option>

                           </select>

                        <?php } ?>
                     
                     </div>
                     <div class="form-group">
                        <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                           <label class="control-label" for="datel">Date From</label>
                           <input class="form-control" type="text" id="datel" readonly />
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div id="datepicker1" class="input-group date" data-date-format="mm-dd-yyyy">
                           <label class="control-label" for="date2">Date To</label>
                           <input class="form-control" type="text" id="date2" readonly />
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="selectCtrl">Price From</label>
                        <select type="text" class="form-control" id="selectCtrl">
                           <option>Price From</option>
                           <option>$100</option>
                           <option>$200</option>
                           <option>$300</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="selectCtrl">Price To</label>
                        <select type="text" class="form-control" id="selectCtrl">
                           <option>Price To</option>
                           <option>$400</option>
                           <option>$500</option>
                           <option>$600</option>
                        </select>
                     </div>
                     <button type="button" class="btn reset">Apply </button>
                     <button type="button" class="btn reset">Reset </button>
                  </div>
               </div>
               <!-- dashboard-top-div -->
               <div class="dashboard-main-div posting-page">
                  <div class="dashbord-main-inner">
                     <div class="content-fullpage mCustomScrollbar">
                        <div class="table-responsive">
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">Icon</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Item</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Date & Time</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Price</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                    </td>
                                    <td>Anee Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>Walmart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Income</td>
                                    <td>+$ 520.00</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                    </td>
                                    <td>Emily Jackson</td>
                                    <td>emilyjacksoon@gmail.com</td>
                                    <td>D-mart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Expense</td>
                                    <td><span class="">-$ 120.00</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                    </td>
                                    <td>Anee Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>Walmart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Income</td>
                                    <td>+$ 520.00</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                    </td>
                                    <td>Emily Jackson</td>
                                    <td>emilyjacksoon@gmail.com</td>
                                    <td>D-mart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Expense</td>
                                    <td><span class="">-$ 120.00</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                    </td>
                                    <td>Anee Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>Walmart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Income</td>
                                    <td>+$ 520.00</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                    </td>
                                    <td>Emily Jackson</td>
                                    <td>emilyjacksoon@gmail.com</td>
                                    <td>D-mart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Expense</td>
                                    <td><span class="">-$ 120.00</span></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                    </td>
                                    <td>Anee Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>Walmart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Income</td>
                                    <td>+$ 520.00</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                    </td>
                                    <td>Emily Jackson</td>
                                    <td>emilyjacksoon@gmail.com</td>
                                    <td>D-mart</td>
                                    <td>Advertising Expenses</td>
                                    <td>Jan 11:00 AM, 2021</td>
                                    <td>Expense</td>
                                    <td><span class="">-$ 120.00</span></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- dashboard-main-inner -->
               </div>
               <!-- dashboard-main-div -->
            </div>
         </div>
      </div>
   </div>

@endsection
@section('scripts')

   <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
   <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
   <script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>
   <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

   
   <script type="text/javascript">
      
        $(function () {
         $('select').selectpicker();
        });
          
   </script>
   <script type="text/javascript">
     
      $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
          if($currEl.val() === $("option:first", $currEl).val()) {
            $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
            $currEl.parent().removeClass('focused');
          } else {
            $('.control-label', $currEl.parent()).css({opacity: 1});
            $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
          }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
      }).trigger('blur');
   </script>
   <script>
      $(function () {
       $("#datepicker").datepicker({ 
      	autoclose: true, 
      	todayHighlight: true
       }).datepicker('update', new Date());
      });
      
      $(function () {
         $("#datepicker1").datepicker({ 
            autoclose: true, 
            todayHighlight: true
         }).datepicker('update', new Date());
      });
   </script>
   
@endsection