@extends('layouts.appAdmin')
  
@section('content')

@section('title', 'Reports')   

   <div class="container-fluid">
      <div class="row">

         @include('layouts.headerAdmin')
         
         <div class="right-main">
            <div class="right-main-inn workout-plan reports">
               <div class="dashboard-top-div">
                  <div class="transaction-page-filter">
                     <div class="form-group">
                        <label class="control-label" for="selectCtrl">Users</label>

                        <?php if( sizeof($users) > 0 ) {?>
                           <select type="text" class="form-control" id="user_sel">

                              @foreach ($users as $user)
                                 <tr>
                                    <option value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }}</option>

                                 </tr>
                              @endforeach

                           </select>

                        <?php }else{ ?>   

                           <select type="text" class="form-control" id="user_sel">                           

                              <option disabled>No Users Yet.</option>

                           </select>

                        <?php } ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label" for="selectCtrl">Categories</label>
                        
                        <?php if( sizeof($categories) > 0 ) {?>
                           <select type="text" class="form-control" id="cat_sel">

                              @foreach ($categories as $cat)
                                 <tr>
                                    <option value="{{ $cat->id }}">{{ $cat->title }}</option>

                                 </tr>
                              @endforeach

                           </select>

                        <?php }else{ ?>   

                           <select type="text" class="form-control" id="user_sel">                           

                              <option disabled>No Categories Yet.</option>

                           </select>

                        <?php } ?>
                     </div>
                     <div class="form-group">
                        <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                           <label class="control-label" for="datel">Date From</label>
                           <input class="form-control" type="text" id="datel" readonly />
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div id="datepicker1" class="input-group date" data-date-format="mm-dd-yyyy">
                           <label class="control-label" for="date2">Date To</label>
                           <input class="form-control" type="text" id="date2" readonly />
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                     </div>
                     <button type="button" class="btn reset">Apply </button>
                     <button type="button" class="btn reset rst">Reset </button>
                  </div>
               </div>
               <!-- dashboard-top-div -->
               <div class="dashboard-main-div posting-page">
                  <div class="dashbord-main-inner">
                     <div class="content-fullpage mCustomScrollbar">
                        <div class="table-responsive">
                           <table class="table table-main">
                              <thead>
                                 <tr>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Account Number</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Tax Payer</th>
                                    <th scope="col">Web File</th>
                                    <th scope="col">EIN</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Anee</td>
                                    <td>Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>Emily</td>
                                    <td>Jackson</td>
                                    <td>Emilyjackson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>john</td>
                                    <td>Watson</td>
                                    <td>johnwatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>Anee</td>
                                    <td>Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>Emily</td>
                                    <td>Jackson</td>
                                    <td>Emilyjackson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>john</td>
                                    <td>Watson</td>
                                    <td>johnwatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>Anee</td>
                                    <td>Watson</td>
                                    <td>aneewatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>Emily</td>
                                    <td>Jackson</td>
                                    <td>Emilyjackson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                                 <tr>
                                    <td>john</td>
                                    <td>Watson</td>
                                    <td>johnwatson@gmail.com</td>
                                    <td>+1 26344-42536</td>
                                    <td>256965231</td>
                                    <td>RT666666</td>
                                    <td>859663521</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- dashboard-main-inner -->
               </div>
               <!-- dashboard-main-div -->
            </div>
         </div>
      </div>
   </div>

@endsection

@section('scripts')  

   <script type="text/javascript">
     
      $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
          if($currEl.val() === $("option:first", $currEl).val()) {
            $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
            $currEl.parent().removeClass('focused');
          } else {
            $('.control-label', $currEl.parent()).css({opacity: 1});
            $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
          }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
      }).trigger('blur');
   </script>
   
   <script>
      $(function () {
       $("#datepicker").datepicker({ 
      	autoclose: true, 
      	todayHighlight: true
       }).datepicker('update', new Date());
      });
      
      $(function () {
         $("#datepicker1").datepicker({ 
            autoclose: true, 
            todayHighlight: true
         }).datepicker('update', new Date());
      });
   </script>

@endsection
