@extends('layouts.app')
 
@section('content')
 
@section('title', 'Income')
   <div id="wrapper" class="income-page">
      @include('layouts.header')
      <div class="Transaction-history transaction_page income_page">
         <div class="container">
            <h2>Income</h2>
            <div class="top-heading">
               <a href="#" class="add-transaction"data-bs-toggle="modal" data-bs-target="#transaction-detail-edit">+</a>
               <select class="form-select" aria-label="Default select example">
                  <option selected>Sort by A - Z</option>
                  <option value="1">A</option>
                  <option value="2">B</option>
                  <option value="3">C</option>
               </select>
               <a href="#" class="print"><img src="{{ asset('images/print.png') }}"></a>
            </div>
            <!-- top-heading -->
            <ul class="transaction-list">
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
            </ul>
         </div>
      </div>
       @include('layouts.footer')
   </div>
   
@endsection
@section('scripts')  
   <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
   </script>
   <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      });
   </script>
@endsection