@extends('layouts.app')
  
@section('content') 

@section('title', 'Change Password')

      <div id="wrapper">
         @include('layouts.header')
         
         <div class="Transaction-history Personal_Information Change-password">
            <div class="container">
               <h3>Change Password</h3>
               <form action="{{route('password-update')}}" method="POST" autocomplete="off">

                  @csrf 
   
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems found.<br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif


                  @if ($message = Session::get('success'))
                     <div class="alert alert-success">
                        <p>{{ $message }}</p>
                     </div>
                  @endif




                  <div class="change-password-box">

                     <div class="form-group">
                        <label class="control-label" for="password">Old Password</label>

                        <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">

                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     </div>

                     <div class="form-group">
                        <label class="control-label" for="new_password">New Password</label>
                        <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">

                        <span toggle="#new_password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     </div>

                     <div class="form-group">
                        <label class="control-label" for="new_confirm_password">Confirm Password</label>

                         <input id="new_confirm_password" type="password" class="form-control" name="confirm_password" autocomplete="current-password">

                         <span toggle="#new_confirm_password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     </div>
                     <button type="submit" class="btn update-pass">Update Password</button>
                  </div>
               </form>
            </div>
         </div>
         @include('layouts.footer')
      </div>

@endsection
@section('scripts')

      
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.6.0/alpine.js"></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script> -->

      <script type="text/javascript">
         
         $('.form-control').on('focus blur change', function (e) {
           var $currEl = $(this);
           
           if($currEl.is('select')) {
             if($currEl.val() === $("option:first", $currEl).val()) {
               $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
               $currEl.parent().removeClass('focused');
             } else {
               $('.control-label', $currEl.parent()).css({opacity: 1});
               $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
             }
           } else {
             $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
           }
         }).trigger('blur');     



         $(".toggle-password").click(function() {

           $(this).toggleClass("fa-eye fa-eye-slash");
           var input = $($(this).attr("toggle"));
           if (input.attr("type") == "password") {
             input.attr("type", "text");
           } else {
             input.attr("type", "password");
           }
         });


         
      </script>
      <script>
         $(document).ready(function(){
           $(".dropbtn").click(function(){
             $(".dropdown-content").toggleClass("show");
           });
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function () {
           $('.menu-btn').click(function(event) {
             $('.navbar-demo').toggleClass('open-nav');
           });
         }); 
         
      </script>
@endsection