@extends('layouts.app')
 
@section('content')
 
@section('title', 'Change Colors')

   <div id="wrapper">
         
      @include('layouts.header')

      <div class="Transaction-history transaction_page transaction-inner-page">
         <div class="container">
            <h2>Car & Truck</h2>
            <div class="top-heading">
               <a href="#" class="add-transaction" data-bs-toggle="modal" data-bs-target="#transaction-detail">+</a>
               <select class="form-select" aria-label="Default select example">
                  <option selected>Sort by A - Z</option>
                  <option value="1">A</option>
                  <option value="2">B</option>
                  <option value="3">C</option>
               </select>
               <a href="#" class="print"><img src="{{ asset('images/print.png') }}"></a>
            </div>
            <!-- top-heading -->
            <ul class="transaction-list">
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Walmart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>Amazon</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                  <div class="right-tansction-detail">
                     <h3>WinCo Food</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">-$300.00</div>
               </li>
               <li>
                  <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                  <div class="right-tansction-detail">
                     <h3>D-Mart</h3>
                     <span>Jan 11:00 AM, 2021</span>
                  </div>
                  <div class="right-transaction-price">$200.00</div>
               </li>
            </ul>
         </div>
      </div>
      @include('layouts.footer')
   </div>
      
   <div class="modal fade" id="transaction-detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Transaction Detail</h5>
               <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <div class="transaction-detail-modal-main">
                  <h2>$20.<small>00$</small></h2>
                  <p>Transport allowance</p>
                  <h3 class="popup-heading">Categories</h3>
                  <div class="categories-div">
                     <label class="categories-checkbox">
                     <input type="checkbox" checked="checked">
                     <span class="checkmark">Advertising Expenses</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Cart & Truck</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Repairs</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Commission & Fees</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Contact Labour</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox" checked="checked">
                     <span class="checkmark">Depletion</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Depreciation</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Insurance</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Emp Benefit Programs</span>
                     </label>
                     <label class="categories-checkbox">
                     <input type="checkbox">
                     <span class="checkmark">Mortgage Interest</span>
                     </label>
                  </div>
                  <!-- categories-div -->
                  <h3 class="popup-heading">Detail</h3>
                  <ul class="transaction-inner-detail">
                     <li>
                        <h4>Payment Detail</h4>
                        <p>January 6th, 11:00 AM</p>
                     </li>
                     <li>
                        <h4>Ref</h4>
                        <p>Payment Invoice #2</p>
                     </li>
                     <li>
                        <h4>Amount</h4>
                        <p>$520.00</p>
                     </li>
                     <li>
                        <div class="file-upload">
                           <div class="file-upload-select">
                              <div class="file-select-button" >Scan/Attach Receipts</div>
                              <div class="file-select-name"></div>
                              <input type="file" name="file-upload-input" id="file-upload-input">
                           </div>
                        </div>
                     </li>
                  </ul>
                  <!-- transaction-inner-detail -->
                  <h3 class="popup-heading">Actions</h3>
                  <div class="addnote-div">
                     <h5>Add a note</h5>
                     <i class="fas fa-edit"></i>
                     <p>Add a note to transactions</p>
                  </div>
                  <!-- addnote-div -->
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary">Save</button>
            </div>
         </div>
      </div>
   </div>
      
@endsection
@section('scripts')
   <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
   </script>
   <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      }); 
      
   </script>
@endsection
