@extends('layouts.app')
 
@section('content')

@section('title', 'Profile')

<div id="wrapper">

   @include('layouts.header')

   <link rel="stylesheet" href="{{ asset('css/range.css') }}">
   <link rel="stylesheet" href="{{ asset('css/semantic.css') }}">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />

      <form action="{{route('profile-update')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
         <div class="Transaction-history Personal_Information transaction_page personal_info">
            <div class="container">
               <h2>Personal Information</h2>

               @if (session('status'))
                  <div class="alert alert-success" role="alert">
                     {{ session('status') }}
                  </div>
               @endif

               @csrf

               <ul class="transaction-list">
                  <li>
                     <div class="user_image_circle">                        
                        <div class="file-input flex items-center">                            
                           <div class="h-12 w-12 rounded-full overflow-hidden bg-gray-100">                           
                                           
                              <div x-show="previewPhoto" class="h-12 w-12 rounded-full overflow-hidden">

                                 <?php if(Auth::user()->profile_image){ ?>

                                    <img class="h-12 w-12 object-cover" src="{{asset(Auth::user()->profile_image)}}" id="user_p_img">

                                 <?php } else { ?>

                                    <img :src="{{ asset('images/profile.png') }}" id="user_p_img" class="h-12 w-12 object-cover">

                                 <?php } ?>
                              </div>
                           </div>
                           <div class="flex items-center edit icon">                            
                              <div class="ml-5 rounded-md shadow-sm">                            
                                 <input @change="updatePreview($refs)"
                                        type="file" 
                                        accept="image/*,capture=camera" 
                                        name="photo" id="photo" 
                                        class="custom">
                                 <label for="photo" class="edit-image">
                                    <img src="{{ asset('images/edit.png') }}">
                                 </label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="right-tansction-detail">
                        
                        <div class="form-group firstname">
                           <label class="control-label" for="firstname">First Name</label>
                           <input type="text" class="form-control" id="firstname" name="first_name" value="{{ Auth::user()->first_name }}" required />
                        </div>
                        <div class="form-group firstname last">
                           <label class="control-label" for="lastname">Last Name</label>
                           <input type="text" class="form-control" id="lastname" name="last_name" value="{{ Auth::user()->last_name }}" required/>
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="phone">Phone Number</label>
                           <input type="text" class="form-control" id="phone" name="phone" readonly value="{{ Auth::user()->phone }}" required/>
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="email">Email Address</label>
                           <input type="email" class="form-control" id="email" name="email" readonly value="{{ Auth::user()->email }}" required/>
                        </div>    

                     </div>
                  </li>
               </ul>
            </div>
            <div class="container basic_info">
               <h2>Basic Information</h2>
               <ul class="transaction-list">
                  <li>
                     <div class="right-tansction-detail">
                       
                        <div class="form-group firstname">
                           <label class="control-label" for="tax-payer">Tax Payer</label>
                           <input type="text" class="form-control" id="tax-payer" name="tax_payer" value="{{ Auth::user()->tax_payer }}" />
                        </div>
                        <div class="form-group firstname last">
                           <label class="control-label" for="webfile">Webfile</label>
                           <input type="text" class="form-control" id="webfile" name="webfile"  value="{{ Auth::user()->webfile }}"/>
                        </div>
                        <div class="form-group">
                           <label class="control-label" for="ein">EIN</label>
                           <input type="text" class="form-control" id="ein" name="ein" value="{{ Auth::user()->ein }}"/>
                        </div>
                        <div class="form-group">
                           <div class="input" id='rangestart'>
                              <label>Company Start Date</label>
                              
                              <input type="text" name="company_start_date" value="{{ Auth::user()->company_start_date }}">
                              <i class=" calendar fas fa-calendar-alt"></i>

                           </div>
                           
                        </div>  

                     </div>
                  </li>
               </ul>
            </div>
            <div class="container basic_info documents">
               <h2>Documents</h2>
               <div class="float-right-add-doc">
                   <h3>Add Documents <a href="#" class="add-icon"data-bs-toggle="modal" data-bs-target="#personal-info">+</a></h3>
               </div>
               <ul class="transaction-list">

                  <?php if( sizeof($users_docs) > 0 ) {?>

                    @foreach ($users_docs as $users_doc)
                       <li>
                          <div class="document-detail">
                             <div class="document-name">
                                 <label>Document Name:</label>
                                 <p>
                                   <?php 

                                      if($users_doc->document_type == 1){
                                         echo "Driving Licence";
                                      }else if($users_doc->document_type == 2){
                                         echo "Passport";
                                      }else if($users_doc->document_type == 3){
                                         echo "Voter Card";
                                      }else if($users_doc->document_type == 4){
                                         echo "Pan Card";
                                      }else{
                                         echo "Others";
                                      }
                                   ?>                                

                                 </p>
                             </div>

                             <!-- <button class="btn download">Download</button> -->

                             <a class="btn download" href="{{asset($users_doc->document_files)}}" download="proposed_file_name">Download</a>


                          </div>
                          <div class="document-detail summary">
                             <label>Summary:</label>
                             <p>{{ $users_doc->document_comments }}</p>
                          </div>
                          <div class="document-detail">
                             <label>Date Uploaded:</label>
                             <p> {{ $users_doc->upload_date }} </p>
                          </div>
                       </li>
                    @endforeach

                  <?php }else{ ?>


                     No Records Yet.


                  <?php } ?>
               </ul>
            </div>
           <div class="button-save-div"><button class="btn download save" type="submit">Save</button></div>
         </div>
      </form>
      @include('layouts.footer')

</div>

<div class="modal fade" id="personal-info" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New Document</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">&times;</button>
            </div>

            <form action="{{route('documents-update')}}" method="POST" enctype="multipart/form-data" autocomplete="off" id="file-upload-form" class="uploader">   

               @if (session('status'))
                  <div class="alert alert-success" role="alert">
                     {{ session('status') }}
                  </div>
               @endif

               @csrf        

               <div class="modal-body">
                   <div class="personal-info-main">
                       <div class="form-group">
                           <label class="control-label" for="selectCtrl">Select Option Value</label>
                           <select type="text" class="form-control" id="selectCtrl" name="document_type" required>
                               <option value="0">Select Option Value</option>
                               <option value="1">Driving Licence</option>
                               <option value="2">Passport</option>
                               <option value="3">Voter Card</option>
                               <option value="4">Pan Card</option>
                           </select>
                       </div>
                       <div class="form-floating">
                           <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea" name="document_comments"></textarea>
                           <label for="floatingTextarea">Comments</label>
                       </div>

                       
                        <input id="file-upload" type="file" name="fileUpload" accept="image/*" required />
                        <label for="file-upload" id="file-drag">
                            <img id="file-image" src="#" alt="Preview" class="hidden">
                            <div id="start">
                                <img src="{{ asset('images/upload-img.png') }}" class="drag-icon">
                                <div>Drag your file and <span id="file-upload-btn" class="btn btn-primary">Select a file</span></div>
                                <div id="notimage" class="hidden">Please select an image</div>
                            </div>
                            <div id="response" class="hidden">
                                <div id="messages"></div>
                                <progress class="progress" id="file-progress" value="0">
                                <span>0</span>%
                                </progress>
                            </div>
                        </label>
                      
                   </div>
                   <!-- personal-info-main -->    
               </div>
               <div class="modal-footer">
                   <button type="submit" class="btn btn-primary">Save</button>
               </div>

            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

   
   <script src="{{ asset('js/semantic.js') }}" ></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
  
  <script type="text/javascript">


   $('.form-control').on('focus blur change', function (e) {
      var $currEl = $(this);
      
      if($currEl.is('select')) {
        if($currEl.val() === $("option:first", $currEl).val()) {
          $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
          $currEl.parent().removeClass('focused');
        } else {
          $('.control-label', $currEl.parent()).css({opacity: 1});
          $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
        }
      } else {
        $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
      }
   }).trigger('blur');  


   photo.onchange = evt => {
      const [file] = photo.files
      if (file) {
         user_p_img.src = URL.createObjectURL(file)
      }
   }

   $('#rangestart').calendar({
     type: 'date',
     defaultDate: '2014-09-12',
   });
   
   
      
   function ekUpload(){
      function Init() {
      
        console.log("Upload Initialised");
        
        var fileSelect    = document.getElementById('file-upload'),
          fileDrag      = document.getElementById('file-drag'),
          submitButton  = document.getElementById('submit-button');
        
        fileSelect.addEventListener('change', fileSelectHandler, false);
        
        // Is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
          // File Drop
          fileDrag.addEventListener('dragover', fileDragHover, false);
          fileDrag.addEventListener('dragleave', fileDragHover, false);
          fileDrag.addEventListener('drop', fileSelectHandler, false);
        }
      }
      
      function fileDragHover(e) {
        var fileDrag = document.getElementById('file-drag');
        
        e.stopPropagation();
        e.preventDefault();
        
        fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
      }
      
      function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        
        // Cancel event and hover styling
        fileDragHover(e);
        
        // Process all File objects
        for (var i = 0, f; f = files[i]; i++) {
          parseFile(f);
          uploadFile(f);
        }
      }
      
      // Output
      function output(msg) {
        // Response
        var m = document.getElementById('messages');
        m.innerHTML = msg;
      }
      
      function parseFile(file) {
      
        console.log(file.name);
        output(
        '<strong>' + encodeURI(file.name) + '</strong>'
        );
        
        // var fileType = file.type;
        // console.log(fileType);
        var imageName = file.name;
        
        var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
        if (isGood) {
          document.getElementById('start').classList.add("hidden");
          document.getElementById('response').classList.remove("hidden");
          document.getElementById('notimage').classList.add("hidden");
          // Thumbnail Preview
          document.getElementById('file-image').classList.remove("hidden");
          document.getElementById('file-image').src = URL.createObjectURL(file);
        }
        else {
          document.getElementById('file-image').classList.add("hidden");
          document.getElementById('notimage').classList.remove("hidden");
          document.getElementById('start').classList.remove("hidden");
          document.getElementById('response').classList.add("hidden");
          document.getElementById("file-upload-form").reset();
        }
      }
      
      function setProgressMaxValue(e) {
        var pBar = document.getElementById('file-progress');
        
        if (e.lengthComputable) {
          pBar.max = e.total;
        }
      }
      
      function updateFileProgress(e) {
        var pBar = document.getElementById('file-progress');
        
        if (e.lengthComputable) {
          pBar.value = e.loaded;
        }
      }
      
      function uploadFile(file) {
      
        var xhr = new XMLHttpRequest(),
        fileInput = document.getElementById('class-roster-file'),
        pBar = document.getElementById('file-progress'),
        fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
          // Check if file is less than x MB
          if (file.size <= fileSizeLimit * 1024 * 1024) {
            // Progress bar
            pBar.style.display = 'inline';
            xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
            xhr.upload.addEventListener('progress', updateFileProgress, false);
          
            // File received / failed
            xhr.onreadystatechange = function(e) {
              if (xhr.readyState == 4) {
                // Everything is good!
          
                // progress.className = (xhr.status == 200 ? "success" : "failure");
                // document.location.reload(true);
              }
            };
        
            // Start upload
            xhr.open('POST', document.getElementById('file-upload-form').action, true);
            xhr.setRequestHeader('X-File-Name', file.name);
            xhr.setRequestHeader('X-File-Size', file.size);
            xhr.setRequestHeader('Content-Type', 'multipart/form-data');
            xhr.send(file);
          } else {
            output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
          }
        }
      }
      
      // Check for the various File API support.
      if (window.File && window.FileList && window.FileReader) {
        Init();
      } else {
        document.getElementById('file-drag').style.display = 'none';
      }
   }
    ekUpload();
      
  </script>
  <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      });
  </script>

@endsection