
 
<div class="top-nav">
    <a href="<?=env('APP_URL')?>"><img src="{{ asset('images/admin/logo.png') }}" alt="logo"></a>
    <div class="right-head-div">
        <div class="dropdown">
            <span>
              
              <?php if(Auth::user()->profile_image){ ?>

                  <img  src="{{asset(Auth::user()->profile_image)}}">

              <?php } else { ?>

                  <img src="{{ asset('images/profile.png') }}">

              <?php } ?>

            </span>
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <h3>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h3>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">               


                <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                    <img src="{{ asset('images/admin/logout.png') }}" alt=""> {{ __('Logout') }}
                </a> 

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
       </div>
    </div>
</div>


<div class="left-bar admin">
    <div class="content-left mCustomScrollbar">
       <ul>
          <li><a href="<?=url('admin/dashboard')?>" class="{{ Route::is('admin.dashboard') ? 'active' : '' }}" ><img src="{{ asset('images/admin/dashboard_active.png') }}" alt="icon">Dashboard</a></li>

          <li><a href="<?=url('admin/users')?>" class="{{ Route::is('admin.users') ? 'active' : '' }}"><img src="{{ asset('images/admin/users_active.png') }}" alt="icon">Users</a></li>

          <li><a href="<?=url('admin/banks')?>" class="{{ Route::is('admin.banks') ? 'active' : '' }}" ><img src="{{ asset('images/admin/banks_active.png') }}" alt="icon">Banks</a></li>

          <li><a href="<?=url('admin/transaction')?>" class="{{ Route::is('admin.transaction') ? 'active' : '' }}" ><img src="{{ asset('images/admin/transaction_active.png')}}" alt="icon">Transaction </a></li>

          <li><a href="<?=url('admin/categories') ?>" class="{{ Route::is('admin.categories.*') ? 'active' : '' }}" ><img src="{{ asset('images/admin/categories_active.png') }}" alt="icon">Categories</a></li>

          <li><a href="<?=url('admin/reports')?>" class="{{ Route::is('admin.reports') ? 'active' : '' }}"><img src="{{ asset('images/admin/reports_active.png') }}" alt="icon">Reports</a></li>

       </ul>
    </div>
</div>

