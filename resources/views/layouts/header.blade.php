<header id="header">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="<?=env('APP_URL')?>"><img src="{{ asset('images/logo.png') }}" alt=""></a>
            <button class="navbar-toggler menu-btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse nav navbar-demo">
                <ul class="navbar-nav nav nav-right">
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('homeC') ? 'active' : '' }} " aria-current="page" href="<?=url('/')?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('banks') ? 'active' : '' }} " href="<?=env('APP_URL')?>banks">Banks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('transactionsController') ? 'active' : '' }} " href="<?=env('APP_URL')?>transactions">Transactions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('report') ? 'active' : '' }} " href="<?=env('APP_URL')?>report">Report</a>
                    </li>
                </ul>
                <button type="" class="menu-btn cross-menu"><i class="fas fa-times"></i></button>
            </div>

            @guest
                @if (Route::has('login'))
                    <ul class="navbar-nav nav nav-right">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                @endif

                @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    </ul>
                @endif
            @else 

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="profile-img">
                        
                        <?php if(Auth::user()->profile_image){ ?>

                            <img  src="{{asset(Auth::user()->profile_image)}}">

                        <?php } else { ?>

                            <img src="{{ asset('images/profile.png') }}">

                        <?php } ?>

                    </span>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="<?=env('APP_URL')?>personal-information">Profile</a></li>
                        <li><a class="dropdown-item" href="<?=env('APP_URL')?>tax-payment">Tax Payment</a></li>
                        <li><a class="dropdown-item" href="<?=env('APP_URL')?>change-password">Change Password</a></li>
                        <li><a class="dropdown-item" href="#">Notifications</a></li>
                        <li><a class="dropdown-item" href="<?=env('APP_URL')?>change-colors">Change Color</a></li>
                        <li><a class="dropdown-item" href="<?=env('APP_URL')?>support">Support</a></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a> 

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>

                <div class="dropdown notification-drop">
                    <button  class="dropbtn"><img src="{{ asset('images/notification.png') }}"></button>
                    <div id="myDropdown" class="dropdown-content">
                        <h3>Notifications</h3>
                        <ul class="notification-list transaction-list">
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                            <li class="minus-price">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Amazon</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li class="minus-price">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>WinCo Food</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$300.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>D-Mart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">$200.00</div>
                            </li>
                        </ul>
                        <!-- notification-list -->
                    </div>
                </div>
                
            @endguest



        </nav>
    </div>
    <!-- Container -->
</header>