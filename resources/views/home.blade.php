@extends('layouts.app')
 
@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->

 
@section('title', 'Home')

<div id="wrapper">

    @include('layouts.header')
    
    <div id="banner">
        <div class="container">
            <div class="left-banner-content">
                <h1>Welcome <b>Anee!</b></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p>
                <div class="total-profit">
                    <p>Total Profit/Loss</p>
                    <h2>$20.525,25</h2>
                </div>
                <!-- total-profit -->
            </div>
            <!-- left-banner-content -->
            <div class="right-banner-graph">
                <img src="{{ asset('images/graph.png') }}">
            </div>
            <!--- right-banner-graph -->
        </div>
        <!-- container -->
    </div>

    <div class="Transaction-history">
        <div class="container">
            <h2>Transaction History <a href="/report-all-categories" class="right-arrow"><i class="fas fa-chevron-right"></i></a></h2>
            <ul class="transaction-list">
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Walmart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Amazon</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>WinCo Food</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>D-Mart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Walmart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Amazon</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>WinCo Food</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>D-Mart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Walmart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Amazon</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>WinCo Food</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>D-Mart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Walmart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>Amazon</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                    <div class="right-tansction-detail">
                        <h3>WinCo Food</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">-$300.00</div>
                </li>
                <li>
                    <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                    <div class="right-tansction-detail">
                        <h3>D-Mart</h3>
                        <span>Jan 11:00 AM, 2021</span>
                    </div>
                    <div class="right-transaction-price">$200.00</div>
                </li>
            </ul>
        </div>
    </div>


    @include('layouts.footer')
    
</div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function(){
          $(".dropbtn").click(function(){
            $(".dropdown-content").toggleClass("show");
          });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
          $('.menu-btn').click(function(event) {
            $('.navbar-demo').toggleClass('open-nav');
          });
        });
    </script>

@endsection
