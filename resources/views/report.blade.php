@extends('layouts.app')
 
@section('content')
 
@section('title', 'Reports')
 
<div id="wrapper">
    @include('layouts.header')

    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="balance-div">
                        <a href="<?=env('APP_URL')?>expense">
                            <div class="left-balance-detail">
                                <small>Balance</small>
                                <h2>$6, 522</h2>
                            </div>
                            <div class="balance-right-icon"><img src="{{ asset('images/balance.png') }}"></div>
                        </a>
                    </div>
                    <!-- balance -->
                </div>
                <div class="col-md-4">
                    <div class="balance-div income-div">
                        <a href="<?=env('APP_URL')?>income">
                            <div class="left-balance-detail">
                                <small>Income</small>
                                <h2>$5, 522</h2>
                            </div>
                            <div class="balance-right-icon"><img src="{{ asset('images/income.png') }}"></div>
                        </a>
                    </div>
                    <!-- balance -->
                </div>
                <div class="col-md-4">
                    <div class="balance-div expense-div">
                        <a href="<?=env('APP_URL')?>expense">
                            <div class="left-balance-detail">
                                <small>Expenses</small>
                                <h2>$2, 002</h2>
                            </div>
                            <div class="balance-right-icon"><img src="{{ asset('images/expenses.png') }}"></div>
                        </a>
                    </div>
                    <!-- balance -->
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="categories-div">
                    <div class="category-top-div">
                        <h3>Categories</h3>
                        <button type="button" class="btn see-all" id="see-all">See All</button>
                    </div>
                    <section class="center slider">


                        <?php if( sizeof($categories) > 0 ) {?>
                           

                            @foreach ($categories as $cat)                               

                                <div class="slick-inner">
                                    <a href="transactions.html">
                                        <div class="category-img"><img src="{{asset($cat->image)}}" height="50" width="50"></div>
                                        <h3>{{ $cat->title }}</h3>
                                        <hr class="slider-sep">
                                        <p>5%</p>
                                    </a>
                                </div>

                            @endforeach
                        <?php }else{ ?>                 
                            No Categories Yet.
                        <?php } ?>                     


                        <!-- <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/commision.png') }}"></div>
                                <h3>Commision & Fees</h3>
                                <hr class="slider-sep">
                                <p>21%</p>
                            </a>
                        </div>
                        
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/depreciation.png') }}"></div>
                                <h3>Depreciation</h3>
                                <hr class="slider-sep">
                                <p>5%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/advertising.png') }}"></div>
                                <h3>Advertising Expenses</h3>
                                <hr class="slider-sep">
                                <p>50%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/car.png') }}"></div>
                                <h3>Car & Truck</h3>
                                <hr class="slider-sep">
                                <p>5%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/contact-labour.png') }}"></div>
                                <h3>Contact Labour</h3>
                                <hr class="slider-sep">
                                <p>50%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/commision.png') }}"></div>
                                <h3>Commision & Fees</h3>
                                <hr class="slider-sep">
                                <p>21%</p>
                            </a>
                        </div>
                        
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/depreciation.png') }}"></div>
                                <h3>Depreciation</h3>
                                <hr class="slider-sep">
                                <p>5%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/advertising.png') }}"></div>
                                <h3>Advertising Expenses</h3>
                                <hr class="slider-sep">
                                <p>50%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/car.png') }}"></div>
                                <h3>Car & Truck</h3>
                                <hr class="slider-sep">
                                <p>5%</p>
                            </a>
                        </div>
                        <div class="slick-inner">
                            <a href="transactions.html">
                                <div class="category-img"><img src="{{ asset('images/contact-labour.png') }}"></div>
                                <h3>Contact Labour</h3>
                                <hr class="slider-sep">
                                <p>50%</p>
                            </a>
                        </div> -->


                    </section>
                </div>
                <!-- categories-div -->
                <div class="report-tabs">
                    <ul class="tabs">
                        <li class="tab-link current" data-tab="Report">Report</li>
                        <li class="tab-link" data-tab="Transaction">Transactions</li>
                        <li class="tab-link" data-tab="Chart">Chart</li>
                    </ul>
                    <div id="Report" class="tab-content current">
                        <ul class="report-list income-list">
                            <li>
                                <h3>Income</h3>
                                <ul class="report_income_list">
                                    <li>
                                        <p>Coaching</p>
                                        <span>1,385.00</span>
                                    </li>
                                    <li>
                                        <p>Bross Brand Scholl</p>
                                        <span>31,192.00</span>
                                    </li>
                                    <li>
                                        <p>Coaching</p>
                                        <span>1,385.00</span>
                                    </li>
                                    <li>
                                        <p>DIY Brand Guide</p>
                                        <span>3,065.00</span>
                                    </li>
                                </ul>
                                <!-- report_income_list -->
                                <div class="total-btm-div">
                                    <p>Total Digital Products</p>
                                    <span>46,768.00</span>
                                </div>
                            </li>
                            <li>
                                <h3>Expenses</h3>
                                <ul class="report_income_list expenses-list">
                                    <li>
                                        <p>Advertising</p>
                                        <span>1,030.00</span>
                                    </li>
                                    <li>
                                        <p>Website</p>
                                        <span>30.00</span>
                                    </li>
                                    <li>
                                        <p>Marketing Express</p>
                                        <span>10.00</span>
                                    </li>
                                </ul>
                                <!-- report_income_list -->
                                <div class="total-btm-div expense-total">
                                    <p>Total Advertising</p>
                                    <span>1,070.00</span>
                                </div>
                            </li>
                            <li>
                                <h3>Profit</h3>
                                <ul class="report_income_list profit-list">
                                    <li>
                                        <p>Cost of goods sold</p>
                                        <span>1,030.00</span>
                                    </li>
                                    <li>
                                        <p>Website</p>
                                        <span>30.00</span>
                                    </li>
                                    <li>
                                        <p>Marketing Express</p>
                                        <span>10.00</span>
                                    </li>
                                </ul>
                                <!-- report_income_list -->
                                <div class="total-btm-div gross-profit-total">
                                    <p>Gross Profit</p>
                                    <span>1,070.00</span>
                                </div>
                            </li>
                        </ul>
                        <!-- report-list -->
                    </div>
                    <div id="Transaction" class="tab-content">
                        <ul class="transaction-list">
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                            <li class="expenses-list">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>D-Mart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$120.00</div>
                            </li>
                            <li class="expenses-list">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>D-Mart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$20.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Easy Day</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Easy Day</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                            <li class="expenses-list">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Amazon</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$120.00</div>
                            </li>
                            <li class="expenses-list">
                                <div class="transaction-arrow down-arrow"><i class="fas fa-arrow-down"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Amazon</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">-$20.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                            <li>
                                <div class="transaction-arrow up-arrow"><i class="fas fa-arrow-up"></i></div>
                                <div class="right-tansction-detail">
                                    <h3>Walmart</h3>
                                    <span>Jan 11:00 AM, 2021</span>
                                </div>
                                <div class="right-transaction-price">+$520.00</div>
                            </li>
                        </ul>
                        <button type="button" class="btn sell-all" id="see-all-trans"> Sell All</button>
                    </div>
                    <div id="Chart" class="tab-content">
                        <div id="chart"><img src="{{ asset('images/graph-image.png') }}" alt=""></div>
                    </div>
                </div>
                <!-- report-tabs -->
            </div>
        </div>
        <!-- container -->
    </div>
    @include('layouts.footer')
</div>

@endsection

@section('scripts')

  <script src="js/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
      $(".center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 6,
        slidesToScroll: 3
      }); 
      
      
      $(document).ready(function(){
      
        $('ul.tabs li').click(function(){
          var tab_id = $(this).attr('data-tab');
      
          $('ul.tabs li').removeClass('current');
          $('.tab-content').removeClass('current');
      
          $(this).addClass('current');
          $("#"+tab_id).addClass('current');
        })
      
      })
  </script>
  <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
  </script>
  <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      }); 
      
        document.getElementById("see-all").onclick = function () {
            location.href = "<?=env('APP_URL')?>report-all-categories";
        };
        document.getElementById("see-all-trans").onclick = function () {
            location.href = "<?=env('APP_URL')?>transaction-list";
        };
  </script>

@endsection