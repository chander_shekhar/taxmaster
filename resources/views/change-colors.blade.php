@extends('layouts.app')
 
@section('content')
 
@section('title', 'Change Colors')

      <div id="wrapper">
         @include('layouts.header')

         <div class="Transaction-history Personal_Information Change-password">
            <div class="container">
               <h3>Change Colors</h3>
               <div class="change-password-box change_color">
                  <h5>Personalisation</h5>
                  <h6>Choose the app theme and accent color.</h6>
                  <div class="mode-div">
                     <label class="mode-select">
                        <input type="radio" checked="checked" name="radio">
                        <span class="checkmark"><img src="{{ asset('images/bright.png') }}">Light</span>
                     </label>
                     <label class="mode-select">
                        <input type="radio" name="radio">
                        <span class="checkmark"><img src="{{ asset('images/dark.png') }}">Dark
                        </span>
                     </label>
                     <label class="mode-select">
                     <input type="radio" name="radio">
                     <span class="checkmark">
                     <img src="{{ asset('images/auto.png') }}">
                     Auto
                     </span>
                     </label>
                  </div>
                  <!-- mode-div -->
                  <div class="mode-div color-select">
                     <label class="mode-select red">
                     <input type="radio" checked="checked" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select blue">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select black">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select seegreen">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select navy">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select gray">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select cyan">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select pink">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                     <label class="mode-select purple">
                     <input type="radio" name="radio1">
                     <span class="checkmark">
                     </span>
                     </label>
                  </div>
                  <!-- mode-div -->
                  <div class="mode-main-div"><img src="{{ asset('images/night-mode.png') }}"> Night Mode</div>
                  <button type="button" class="btn-apply">Apply</button>
               </div>
            </div>
         </div>
         @include('layouts.footer')
      </div>

@endsection
@section('scripts')
      
   <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.6.0/alpine.js"></script>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
   <script type="text/javascript">
      
      $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
          if($currEl.val() === $("option:first", $currEl).val()) {
            $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
            $currEl.parent().removeClass('focused');
          } else {
            $('.control-label', $currEl.parent()).css({opacity: 1});
            $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
          }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
      }).trigger('blur');
      
      
      /***** Upload Image ******/
      function imageData(url) {
      const originalUrl = url || '';
      return {
      previewPhoto: originalUrl,
      fileName: null,
      emptyText: originalUrl ? 'No new file chosen' : 'No file chosen',
      updatePreview($refs) {
      var reader,
          files = $refs.input.files;
      reader = new FileReader();
      reader.onload = (e) => {
        this.previewPhoto = e.target.result;
        this.fileName = files[0].name;
      };
      reader.readAsDataURL(files[0]);
      },
      clearPreview($refs) {
      $refs.input.value = null;
      this.previewPhoto = originalUrl;
      this.fileName = false;
      }
      };
      }
      
      
      
      $(function() {
      $( ".calendar" ).datepicker({
      dateFormat: 'dd/mm/yy',
      firstDay: 1
      });
      
      $(document).on('click', '.date-picker .input', function(e){
      var $me = $(this),
        $parent = $me.parents('.date-picker');
      $parent.toggleClass('open');
      });
      
      
      $(".calendar").on("change",function(){
      var $me = $(this),
        $selected = $me.val(),
        $parent = $me.parents('.date-picker');
      $parent.find('.result').children('span').html($selected);
      });
      });
      
      
      
      
      
      
      // File Upload
      // 
      function ekUpload(){
      function Init() {
      
      console.log("Upload Initialised");
      
      var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');
      
      fileSelect.addEventListener('change', fileSelectHandler, false);
      
      // Is XHR2 available?
      var xhr = new XMLHttpRequest();
      if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
      }
      }
      
      function fileDragHover(e) {
      var fileDrag = document.getElementById('file-drag');
      
      e.stopPropagation();
      e.preventDefault();
      
      fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
      }
      
      function fileSelectHandler(e) {
      // Fetch FileList object
      var files = e.target.files || e.dataTransfer.files;
      
      // Cancel event and hover styling
      fileDragHover(e);
      
      // Process all File objects
      for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
      }
      }
      
      // Output
      function output(msg) {
      // Response
      var m = document.getElementById('messages');
      m.innerHTML = msg;
      }
      
      function parseFile(file) {
      
      console.log(file.name);
      output(
      '<strong>' + encodeURI(file.name) + '</strong>'
      );
      
      // var fileType = file.type;
      // console.log(fileType);
      var imageName = file.name;
      
      var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
      if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
      }
      else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
      }
      }
      
      function setProgressMaxValue(e) {
      var pBar = document.getElementById('file-progress');
      
      if (e.lengthComputable) {
      pBar.max = e.total;
      }
      }
      
      function updateFileProgress(e) {
      var pBar = document.getElementById('file-progress');
      
      if (e.lengthComputable) {
      pBar.value = e.loaded;
      }
      }
      
      function uploadFile(file) {
      
      var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
      if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);
      
        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!
      
            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };
      
        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
      }
      }
      
      // Check for the various File API support.
      if (window.File && window.FileList && window.FileReader) {
      Init();
      } else {
      document.getElementById('file-drag').style.display = 'none';
      }
      }
      ekUpload();
      
   </script>
   <script>
      $(document).ready(function(){
        $(".dropbtn").click(function(){
          $(".dropdown-content").toggleClass("show");
        });
      });
   </script>
   <script type="text/javascript">
      $(document).ready(function () {
        $('.menu-btn').click(function(event) {
          $('.navbar-demo').toggleClass('open-nav');
        });
      }); 
      
   </script>
@endsection
