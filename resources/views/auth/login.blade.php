@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div> 

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@section('title', 'Login')

<div class="login-main">
    <div class="login-left">
        <div class="login-left-inn">
            <div class="logo-div">
               <a href=""><img src="{{ asset('images/logo.png') }}"></a>
            </div>
            <div class="login-right-inner">
                <h3>Welcome back!</h3>
                <h4>Sign in to continue using Tax Master</h4>
                 <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label" for="email">Email</label>                       

                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror


                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>                       

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        <!-- <span class="password-icon"><img src="{{ asset('images/password.png') }}"></span> -->
                        <span  id="btnToggle" class="password-icon toggle"><i id="eyeIcon" class="fa fa-eye"></i></span>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="form-group form-check"> 

                        <label class="rem">{{ __('Remember Me') }}
                             <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>                            
                            <span class="checkmark"></span>
                        </label>                   

                        @if (Route::has('password.request'))
                            <a class="forgot" href="{{ route('password.request') }}">
                                {{ __('Forgot Password?') }}
                            </a>
                        @endif

                    </div>
 

                    <button type="submit" class="btn btn-primary login">{{ __('Sign In') }}</button>                 

                    <h6 class="or-login">Or log in with</h6>

                    <div class="social-links-for-login">

                        <a class="btn" href="{{ route('social.oauth', 'google') }}" ><img class="google-icon" src="{{ asset('images/google.png') }}" alt=""></a>

                        <a class="btn" href="{{ route('social.oauth', 'apple') }}" ><i class="fab fa-apple"></i></a>
                        
                        <a class="btn" href="{{ route('social.oauth', 'facebook') }}" ><i class="fab fa-facebook-f"></i></a>

                    </div>

                    <div class="accounthave" style="display: none;">
                        <h6>Don't have an account? <a class="nav-link" href="{{ route('register') }}"> Create a new account</a></h6>
                    </div>

                </form>
           </div>
        </div>
    </div>
    <div class="login-right">
        <img src="{{ asset('images/login-vector.png') }}" alt="">
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
         
    $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
       
        if($currEl.is('select')) {
            if($currEl.val() === $("option:first", $currEl).val()) {
                $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
                $currEl.parent().removeClass('focused');
            }else{
                $('.control-label', $currEl.parent()).css({opacity: 1});
                $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
            } 
        }else{
            $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
    }).trigger('blur');

    let passwordInput = document.getElementById('password'),
        toggle        = document.getElementById('btnToggle'),
        icon          = document.getElementById('eyeIcon');

    function togglePassword() {
      if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        icon.classList.add("fa-eye-slash");
      } else {
        passwordInput.type = 'password';
        icon.classList.remove("fa-eye-slash");
      }
    }

    function checkInput() {
      if (passwordInput.value === '') {
      toggle.style.display = 'none';
       passwordInput.type = 'password';
      } else {
       toggle.style.display = 'block';
      }
    }

    toggle.addEventListener('click', togglePassword, false);
    passwordInput.addEventListener('keyup', checkInput, false);

</script>

@endsection
