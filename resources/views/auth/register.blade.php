@extends('layouts.app')

@section('content') 
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@section('title', 'Register')

<div class="login-main signupmain">
    <div class="login-left">
        <div class="login-left-inn">
            <div class="logo-div">
                <a href=""><img src="{{ asset('images/logo.png') }}"></a>
            </div>
            <div class="login-right-inner">
                <h3>Join Tax Master</h3>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group firstname">
                        <label class="control-label" for="first_name">{{ __('First Name') }}</label>                      
                        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>

                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="form-group firstname last">                       

                        <label class="control-label" for="last_name">{{ __('Last Name') }}</label>                      
                        <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                        @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror



                    </div>
                    <div class="form-group">
                        <label class="control-label" for="phone">Phone Number</label>                      

                        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                        @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email">Email Address</label>                        

                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>                       


                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        <!-- <span class="password-icon"><img src="{{ asset('images/password.png') }}"></span> -->
                        <span  id="btnToggle" class="password-icon toggle"><i id="eyeIcon" class="fa fa-eye"></i></span>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>


                    <div class="form-group">
                        <label class="control-label" for="password-confirm">{{ __('Confirm Password') }}</label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                        <!-- <span class="password-icon"><img src="{{ asset('images/password.png') }}"></span> -->
                        <span  id="cbtnToggle" class="password-icon toggle"><i id="ceyeIcon" class="fa fa-eye"></i></span>
                        
                    </div>

                    <div class="form-group form-check">
                        <label class="rem">I agree to <a href="#" target="_blank">Terms & Conditions</a>
                            <input type="checkbox" name="terms_agree">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <button type="submit" id="create-account" class="btn login" disabled="disabled">Sign Up</button>


                    <h6 class="or-login">Or sign up with</h6>
                    <div class="social-links-for-login">
                        <button class="btn"><img class="google-icon" src="{{ asset('images/google.png') }}" alt=""></button>
                        <button class="btn"><i class="fab fa-apple"></i></button>
                        <button class="btn"><i class="fab fa-facebook-f"></i></button>
                    </div>
                    <div class="accounthave">
                        <h6>Already have an account? <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }} </a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="login-right">
        <img src="{{ asset('images/login-vector.png') }}" alt="">
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
    $('.form-control').on('focus blur change', function (e) {
        var $currEl = $(this);
        
        if($currEl.is('select')) {
            if($currEl.val() === $("option:first", $currEl).val()) {
                $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
                $currEl.parent().removeClass('focused');
            }else{
                $('.control-label', $currEl.parent()).css({opacity: 1});
                $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
            }
        } else {
          $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
        }
    }).trigger('blur');


    let passwordInput  = document.getElementById('password'),
        toggle         = document.getElementById('btnToggle'),
        icon           = document.getElementById('eyeIcon'),

        cpasswordInput = document.getElementById('password-confirm'),
        ctoggle        = document.getElementById('cbtnToggle'),
        cicon          = document.getElementById('ceyeIcon');

    function togglePassword() {
      if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        icon.classList.add("fa-eye-slash");
      } else {
        passwordInput.type = 'password';
        icon.classList.remove("fa-eye-slash");
      }
    }

    function checkInput() {
      if (passwordInput.value === '') {
      toggle.style.display = 'none';
       passwordInput.type = 'password';
      } else {
       toggle.style.display = 'block';
      }
    }

    toggle.addEventListener('click', togglePassword, false);
    passwordInput.addEventListener('keyup', checkInput, false);


    function ctogglePassword() {
      if (cpasswordInput.type === 'password') {
        cpasswordInput.type = 'text';
        cicon.classList.add("fa-eye-slash");
      } else {
        cpasswordInput.type = 'password';
        cicon.classList.remove("fa-eye-slash");
      }
    }

    function ccheckInput() {
      if (cpasswordInput.value === '') {
      ctoggle.style.display = 'none';
       cpasswordInput.type = 'password';
      } else {
       ctoggle.style.display = 'block';
      }
    }

    ctoggle.addEventListener('click', ctogglePassword, false);
    cpasswordInput.addEventListener('keyup', ccheckInput, false);


    $('input[name=terms_agree]').click(function (params) {
        if ($(this).is(':checked')) {
            console.log('Checked');
            $('#create-account').removeClass('disabled');
            $('#create-account').removeAttr('disabled');
        } else {
            console.log('Un-Checked');
            $('#create-account').addClass('disabled');
            $('#create-account').attr('disabled', 'disabled');
        }            
    });



</script>


@endsection
