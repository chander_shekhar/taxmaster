@extends('layouts.app')
 
@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@section('title', 'Forgot Password')

<div class="login-main forgot-page">
    <div class="login-left">
        <div class="login-left-inn">
            <div class="logo-div">
              <a href=""><img src="{{ asset('images/logo.png') }}"></a>
            </div>
            <div class="login-right-inner">
                <h3>Forgot Password</h3>
                <h4>Enter the email address associated with your  account
                and we’ll send you a link to reset your password...</h4>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <label class="control-label" for="email">Email</label>                       

                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <button type="submit" class="btn btn-primary login">{{ __('Send Link') }} </button>

                    <div class="accounthave"><h6><a href=""> Reset Your Password</a></h6></div>
                </form>
            </div>
        </div>
    </div>
    <div class="login-right">
        <img src="{{ asset('images/forgot-password.png') }}" alt="">
    </div>
</div>

@endsection

@section('scripts')

    
<script type="text/javascript">
  $('.form-control').on('focus blur change', function (e) {
    var $currEl = $(this);
    
    if($currEl.is('select')) {
      if($currEl.val() === $("option:first", $currEl).val()) {
        $('.control-label', $currEl.parent()).animate({opacity: 0}, 240);
        $currEl.parent().removeClass('focused');
      } else {
        $('.control-label', $currEl.parent()).css({opacity: 1});
        $currEl.parents('.form-group').toggleClass('focused', ((e.type === 'focus' || this.value.length > 0) && ($currEl.val() !== $("option:first", $currEl).val())));
      }
    } else {
      $currEl.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }
  }).trigger('blur');
</script>
@endsection
